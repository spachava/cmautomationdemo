package com.mckesson.mhs.cm.pageobjects;

/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 13 Dec, 2012
 * Time: 6:08:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContractsObjects {

    public static final String CREATE_CONTRACT_BTN_ID = "btnCreateContract";
    public static final String CREATE_AMENDMENTS_BTN_ID = "btnCreateAmendments";
    public static final String COURIER_BTN_ID = "btnCourier";
    public static final String SIGN_BTN_ID = "btnSignature";
    public static final String STATUS_BTN_ID = "btnStatus";
    public static final String ROLLOVER_BTN_ID = "btnRollover";
    public static final String TRANSMITTAL_BTN_ID = "btnTransmittal";
    public static final String NEGO_BTN_ID = "btnNego";
    public static final String SEND_NEGO_BTN_ID = "btnSendNegotiation";
    public static final String REOPEN_NEGO_BTN_ID = "btnReopenNegotiation";
    public static final String CLOSE_NEGO_BTN_ID = "btnCloseNegotiation";
    public static final String ACTIVATE_BTN_ID = "btnActivate";
    public static final String DEACTIVATE_BTN_ID = "btnDeactivate";
    public static final String EXECUTE_BTN_ID = "btnNeverExecute";
    public static final String BULK_STATUS_CHANGE_BTN_ID = "btnBulkStatusChange";
    public static final String BULK_BYPASS_REVIEW_BTN_ID = "btnBulkBypassReview";
    public static final String ADD_CONTRACT_BTN_ID = "btnAddContract";
    public static final String CHOOSE_ALL_BTN_ID = "btnChooseAll";
    public static final String CONTRACTOR_DROPDOWN_NAME = "contractorid";
    public static final String ADD_PROVIDER_BTN_ID = "btnAddProvider";
    public static final String SEND_NEGO_TEXT = "Send Negotiation";
    public static final String SET_CONTACT_BTN_ID = "btnSetContact";
    public static final String RENEWAL_MODE_ID = "renewalmodeid";
    public static final String SET_ALL_CONTACTS_BTN_ID = "btnSetAllContacts";
    public static final String CREATE_TEMPLATE_BTN_ID = "btnCreateDocSet";
}
