package com.mckesson.mhs.cm.pageobjects;

/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 18 Dec, 2012
 * Time: 5:17:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminObjects {

    public static final String MAIN_CREATE_BTN_ID = "btnMainCreate";
    public static final String NAME_INPUT_ID = "fieldname";
    public static final String DESC_INPUT_ID = "description";
    public static final String PROVIDER_NAME_INPUT_ID = "provname";
    public static final String FIELDNAME_TEXT_ID = "fieldname";
    public static final String FIELDLABEL_TEXT_ID = "fieldlabel";
    public static final String FIELDTYPE_SELECT_NAME = "fieldtype";
    public static String ROLE_NAME_TEXT_ID = "rolename";
    public static  String USER_TAB = "Users";
    public static  String ADD_USER = "btnAdd";
    public static  String REMOVE_USER = "btnRemove";
    public static  String ROLES_TAB = "Roles";
    public static  final String ASSOCIATION_TAB = "Associations";
    public static final String INPUT = "input";
    public static final String VALUE = "value";
   }

