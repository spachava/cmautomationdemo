package com.mckesson.mhs.cm.pageobjects;

/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 23 Apr, 2013
 * Time: 3:47:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProviderObjects {
    public static final String MAIN_CREATE_BTN_ID = "btnMainCreate";
    public static final String CREATE_BTN_ID = "btnCreate";
    public static final String PROVIDER_NAME_TEXT_ID = "provname";
    public static final String ALT_NAME_1 = "altnm_1";
    public static final String PROV_TYPE = "provtype";
    public static final String DETAILS_TAB = "Details";

    public static final String CRED_ID = "credid";
    public static final String CRED_DATE_NAME = "creddate";
    public static final String LOCATION_TAB = "Location";
    public static final String COUNTY_TEXT_ID = "county";
    public static final String LOCALITY_TEXT_ID = "locality";
    public static final String REGION_TEXT_ID = "label_1";
    public static final String ADDRESS_TAB = "Address";
    public static final String CONTACTS_TAB = "Contacts";

    public static final String MEMOS_TAB = "Memos";
    public static final String MANAGEMENT_TAB = "Management";
    public static final String CODES_TAB = "Codes";
    public static final String GUIDELINE_TAB = "Guideline";
    public static final String CATEGORIES_TAB = "Categories";
    public static final String SUB_PROVIDERS_TAB = "Subproviders";
    public static final String ADD_ADDRESS_BTN_ID = "btnAddAddress";
    public static final String ADD_CONTACT_BTN_ID = "btnAddContact";
    public static final String CREATE_TAX_BTN_ID = "btnCreatex";
    public static final String ADD_PROVIDER_BTN_ID = "btnAddProvider";
    
}
