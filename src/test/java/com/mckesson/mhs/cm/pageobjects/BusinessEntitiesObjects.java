package com.mckesson.mhs.cm.pageobjects;

public class BusinessEntitiesObjects {
	public static final String BE_TEXT = "Business Entities";
	public static final String BE_NAME_ID = "contractorName";
	public static final String BE_LABEL_ID = "label";
	public static final String BE_TAX_ID = "contractorTaxId";
	public static final String BE_INITIAL_ID = "contractorInitial";
	public static final String DISABLE_BTN_ID = "btnDisable";
	public static final String STATE_TEXT = "State";
	public static final String STATEDROPDOWN_ID = "StateDropDown";
	public static final String STATE_ACTIVE = "Active";
	public static final String STATE_DISABLED = "Disabled";
	public static final String STATE_ALL = "All";
	public static final String ENABLE_BTN_ID = "btnEnable"; 
	
}
