/**
 *
 */
package com.mckesson.mhs.cm.pageobjects;

/**
 * @author dsamantra
 */
public class CommonObjects {
	
	public static final String BUTTON_PREVIEW = "btnPreview";
    public static final String SEARCH_DROPDOWN_ID = "SearchDropDown";
    public static final String SEARCH_TEXT_ID = "SearchText";
    public static final String SEARCH_BTN_ID = "btnSearch";
    public static final String FILTER_BTN_ID = "btnFilter";
    public static final String CREATE_BTN_ID = "btnCreate";
    public static final String VIEW_VAR_REF_BTN_ID = "btnVariableReference";
    public static final String REVISE_BTN_ID = "btnRevise";
    public static final String COPY_BTN_ID = "btnCopy";
    public static final String EDIT_BTN_ID = "btnEdit";
    public static final String DELETE_BTN_ID = "btnDelete";
    public static final String REQUEST_REVIEW_BTN_ID = "btnRequestReview";
    public static final String REQUEST_APPROVAL_BTN_ID = "btnRequestApproval";
    public static final String BYPASS_REVIEW_BTN_ID = "btnBypassReview";
    public static final String PUBLISH_BTN_ID = "btnPublish";
    public static final String RETIRE_BTN_ID = "btnRetire";
    public static final String UPDATE_BTN_ID = "btnUpdate";
    public static final String MEMOS_BTN_ID = "btnMemos";
    public static final String MEMOS_DESC_BTN_ID = "memos";
    public static final String ADD_MEMO_BTN_ID = "btnAddMemo";
    public static final String MEMO_INPUT_TEXT = "memotext";
    public static final String FLAG_BTN_ID = "btnFlag";
    public static final String COMPARE_BTN_ID = "btnCompare";
    public static final String CHECKIN_BTN_ID = "btnCheckIn";
    public static final String CHECKOUT_BTN_ID = "btnCheckOut";
    public static final String CANCEL_CHECKOUT_BTN_ID = "btnCancelCheckOut";
    public static final String CATEGORIZE_BTN_ID = "btnCategorize";
    public static final String DEPARTMENT_BTN_ID = "btnDepartment";
    public static final String VIEW_CHECKOUTS_BTN_ID = "btnViewCheckouts";
    public static final String DEPARTMENTALIZE_BTN_ID = "btnDepartmentalize";
    public static final String FILE_ID = "file";
    public static final String OKBK_BTN_ID = "btnOKbk";
    public static final String OK_BTN_ID = "btnOK";
    public static final String SAVE_BTN_ID = "btnSave";
    public static final String CATEGORIZE_RADIO_ID = "radiocategorize";
    public static final String ADD_CATEGORY_BTN_ID = "btnAddCategory";
    public static final String CHOOSE_SELECTED_BTN_ID = "btnChooseSelected";
    public static final String VIEW_AS_WORD_LINK_TEXT = "View as Word";
    public static final String GENERATE_PDF_LINK_TEXT = "Generate PDF";
    public static final String DOWNLOAD_BTN_ID = "btnDownload";
    public static final String WIZ_FINISH_BTN_ID = "btnSequentialWizFinish";
    public static final String STATE_FILTER_DROPDOWN_ID = "State_filter";
    public static final String ADD_CONTENTS_BTN_ID = "btnAddContents";
    public static final String CONTENTS_TEXT = "Contents";
    public static final String HISTORY_LINK_TEXT = "History";
    public static final String DIFF_BTN_ID = "btnDiff";
    public static final String SEND_BTN_ID = "btnSend";
    public static final String CALENDAR_BTN_ID = "btnCalendar";
    public static final String DUEDATE_INPUT_NAME = "DueDate";
    public static final String NOTIF_MSG_INPUT_NAME = "notifmsg";
    public static final String REVIEW_BTN_ID = "btnReview";
    public static final String SAVE_COMPLETE_BTN_ID = "btnSaveAndComplete";
    public static final String MSG_TEXT_ID = "messagetext";
    public static final String SELECTION_INPUT_NAME = "selectionids";
    public static final String CHOOSER_BTN_ID = "btnChooser";
    public static final String NAME_TEXT_ID = "name";
    public static final String MAIN_CREATE_BTN_ID = "btnMainCreate";
    public static final String PDF_ID = "pdf";
    public static final String WORD_ID = "word";
    public static final String DIALOG_CLOSE_BTN_ID = "btnDialogClose";
    public static final String STATUS_FILTER_DROPDOWN_ID = "Status_filter";
    public static final String IS_FLAGGED_INPUT_ID = "isFlagged";
    public static final String ADD_REVIEWER_BTN_ID = "btnAddReviewer";
    public static final String ADD_APPROVER_BTN_ID = "btnAddApprover";
    public static final String DETAILS_ID = "details";
    public static final String ALTERNATIVES_ID = "alternates";
    public static final String RULES_ID = "rules";
    public static final String MEMOS_ID = "memos";
    public static final String GUIDELINE_TEXT_ID = "guideline";
    public static final String KEYWORD_TEXT_NAME = "keywords";
    public static final String BROWSE_DROPDOWN_ID = "BrowseDropDown";
    public static final String CATEGORY_FILTER_ID = "Category_filter";
    public static final String CLOSE_REVIEW_BTN_ID = "btnCloseReview";

}
