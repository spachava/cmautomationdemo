package com.mckesson.mhs.cm.pageobjects;

public class HomePageObjects {

    // Contracts and its sub menus
    public static final String CONTRACTS_MENU_LINK_TEXT = "Contracts";
    public static final String CONTRACTS_SUBMENU_LINK_TEXT = "Contracts";
    public static final String CONTRACT_TEMPLATES_SUBMENU_LINK_TEXT = "Contract Templates";
    public static final String CONTRACT_QUERIES_SUBMENU_LINK_TEXT = "Contract Queries";
    public static final String COURIER_SUBMENU_LINK_TEXT = "Courier";
    public static final String NEGOTIATIONS_SUBMENU_LINK_TEXT = "Negotiations";

    // Language and its sub menus
    public static final String LANGUAGE_MENU_LINK_TEXT = "Language";
    public static final String DOCUEMNTS_SUBMENU_LINK_TEXT = "Documents";
    public static final String TERMS_SUBMENU_LINK_TEXT = "Terms";

    // Providers and its sub menus
    public static final String PROVIDERS_MENU_LINK_TEXT = "Providers";
    public static final String PROVIDERS_SUBMENU_LINK_TEXT = "Providers";
    public static final String PROVIDER_QUERIES_SUBMENU_LINK_TEXT = "Provider Queries";

    // Review/Approval and its sub menus
    public static final String REVIEW_APPROVAL_MENU_LINK_TEXT = "Review/Approval";
    public static final String REVIEW_TASKS_SUBMENU_LINK_TEXT = "Review Tasks";
    public static final String APPROVAL_TASKS_SUBMENU_LINK_TEXT = "Approval Tasks";
    public static final String REVIEW_REQUESTS_SUBMENU_LINK_TEXT = "Review Requests";
    public static final String APPROVAL_REQUESTS_SUBMENU_LINK_TEXT = "Approval Requests";

    // Tools and its sub menus
    public static final String TOOLS_MENU_LINK_TEXT = "Tools";
    public static final String CREATE_PRINT_FILE_SUBMENU_LINK_TEXT = "Create Print File";
    public static final String AUDIT_LOG_SUBMENU_LINK_TEXT = "Audit Log";
    public static final String CONTENT_EXPORT_SUBMENU_LINK_TEXT = "Content Export";
    public static final String CONTENT_IMPORT_SUBMENU_LINK_TEXT = "Content Import";
    public static final String PROJECT_TEMPLATE_EXPORT_SUBMENU_LINK_TEXT = "Project Template Export";
    public static final String PROJECT_TEMPLATE_IMPORT_SUBMENU_LINK_TEXT = "Project Template Import";

    // Admin and its sub menus
    public static final String ADMIN_MENU_LINK_TEXT = "Admin";
    public static final String USERS_SUBMENU_LINK_TEXT = "Users";
    public static final String ROLES_SUBMENU_LINK_TEXT = "Roles";
    public static final String BUSINESS_ENTITIES_SUBMENU_LINK_TEXT = "Business Entities";
    public static final String CUSTOM_FIELDS_SUBMENU_LINK_TEXT = "Custom Fields";
    public static final String CUSTOM_FORMS_SUBMENU_LINK_TEXT = "Custom Forms";
    public static final String NOTIFICATION_GROUPS_SUBMENU_LINK_TEXT = "Notification Groups";
    public static final String TYPES_SUBMENU_LINK_TEXT = "Types";
    public static final String DEPARTMENTS_SUBMENU_LINK_TEXT = "Departments";
    public static final String CATEGORIES_SUBMENU_LINK_TEXT = "Categories";
    public static final String ABOUT_SUBMENU_LINK_TEXT = "About Contract Manager";

}
