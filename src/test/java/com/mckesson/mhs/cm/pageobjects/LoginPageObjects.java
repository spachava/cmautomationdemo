/**
 *
 */
package com.mckesson.mhs.cm.pageobjects;

/**
 * @author dsamantra
 */
public class LoginPageObjects {

    public static final String USERNAME_TEXT_ID = "username";
    public static final String PASSWORD_TEXT_ID = "password";
    public static final String LOGIN_BTN_ID = "btnLogin";
    public static final String LOGOUT_SPAN_TEXT = "Logout";
    public static final String CHANGE_SPAN_TEXT = "Change Password";
    
}
