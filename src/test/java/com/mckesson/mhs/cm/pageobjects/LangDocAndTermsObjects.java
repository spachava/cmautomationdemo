package com.mckesson.mhs.cm.pageobjects;

/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 13 Dec, 2012
 * Time: 5:41:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class LangDocAndTermsObjects {

    public static final String CREATE_DOCUMENT_BTN_ID = "btnCreateDocument";
    public static final String DOCUMENT_CHOOSER_BTN_ID = "btnChooser";
    public static final String CREATE_TERM_BTN_ID = "btnCreateTerm";
    public static final String ADD_TERM_BTN_ID = "btnAddTerm";
    public static final String GENERATE_DOCUMENT_SPAN_TEXT = "Generate Document";
    public static final String IMPORT_DOC_BTN_ID = "btnImportDoc";
    public static final String CATEGORIES_TD_TEXT = "Categories";
    public static final String LIBRARY_TD_TEXT = "Library";
    public static final String TERM_NAME_INPUT_ID = "termname";
    public static final String ADD_REVIEWER_ID = "btnLangAddReviewer";
    public static final String ADD_APPROVER_ID = "btnLangAddApprover";
    public static final String IS_NOTIFY_DOC_INPUT_ID = "isNotifyDoc";
    public static final String CREATE_RULE_BTN_ID = "btnCreateRule";
    public static final String CHOICE_DROPDOWN_ID = "depChoice";
}
