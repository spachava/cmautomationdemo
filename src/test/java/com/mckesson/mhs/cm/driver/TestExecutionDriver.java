package com.mckesson.mhs.cm.driver;

import com.mckesson.mhs.cm.utils.CommonUtil;
import com.mckesson.mhs.cm.utils.PropertyManager;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.TestNG;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.uncommons.reportng.HTMLReporter;
import org.uncommons.reportng.JUnitXMLReporter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * This class contains the test execution method that would trigger the other
 * classes and methods
 *
 * @author Siva Alla
 */

public class TestExecutionDriver {

    public static WebDriver driver = null;
    private static final String[] modules = {
        "com.mckesson.mhs.cm.tests.tools.printfile",
    };
    
    @BeforeClass
    public void before(){
        System.out.println("Tests will be initiated now");
        //SauceLabs Code here
    }
    
    @Test
    public void runTests() {
    	HTMLReporter HTMLReporter=new HTMLReporter();
    	JUnitXMLReporter JUnitXMLReporter= new JUnitXMLReporter();
        List<XmlSuite> xmlSuites = new ArrayList<XmlSuite>();
        System.out.println("****************");
        for(String module: modules){
            if(PropertyManager.getProperty(module)!=null && PropertyManager.getProperty(module).equalsIgnoreCase("yes")){
                 xmlSuites.add(prepareTestSuite(module));
                 Reporter.log("the test suite is "+module);
            }
        }

        TestNG tng = new TestNG();
        tng.setUseDefaultListeners(false);
        tng.addListener(HTMLReporter);
        tng.addListener(JUnitXMLReporter);
        tng.setXmlSuites(xmlSuites);
        tng.run();
    }

    @AfterClass
    public void after() throws IOException {
        CommonUtil.renameFolder("Test-Results-");
    }

    private XmlSuite prepareTestSuite(String module) {
    	String[] testName=null;
        List<XmlPackage> packageList = new ArrayList<XmlPackage>();
        XmlSuite xmlSuite = new XmlSuite();
        xmlSuite.setName(module);
        XmlTest xmlTest = new XmlTest(xmlSuite);
        testName=module.split("\\.");
        xmlTest.setName(testName[testName.length-2].toUpperCase()+" - "+testName[testName.length-1].toUpperCase());
        XmlPackage xmlPackage = new XmlPackage();
        xmlPackage.setName(module);
        packageList.add(xmlPackage);
        xmlTest.setPackages(packageList);
        return xmlSuite;
    }

}
