package com.mckesson.mhs.cm.utils;

import com.mckesson.mhs.cm.pageobjects.*;
import com.mckesson.mhs.cm.tests.GenericTest;
import org.apache.log4j.Logger;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.mckesson.mhs.cm.utils.CommonUtil.*;
import static org.fest.assertions.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.*;

public class CommonWorkFlowUtil extends GenericTest {
	
	static String winHandle;
	public static final String startDate=CommonUtil.currentDateAndTimeforDateFormat("MM/dd/yyyy");
	public static final String endDate="12/12/9999";
    public static Process p;
    static final Logger logger = Logger.getLogger(CommonWorkFlowUtil.class);

            CommonUtil CommonUtil1=new CommonUtil();
	
	public void performSearch(String SearchDropDownValue,String SearchFor){
		
		initFluent(driver);
		sleepThread(200);
		await().atMost(30000).until("#"+CommonObjects.SEARCH_DROPDOWN_ID).isPresent();
		find("#"+CommonObjects.SEARCH_DROPDOWN_ID).find("option", withText(SearchDropDownValue)).click();
		await().atMost(5000).until("#SearchText").isPresent();
		find("#SearchText").text(SearchFor);
		click("#"+CommonObjects.SEARCH_BTN_ID);
		sleepThread(500);
		CommonUtil1.Sync();

	
	}
	
	public void applyFilter(String FilterName,String Value){
		
		initFluent(driver);
		find("select",withName().contains(FilterName)).find("option", withText(Value)).click();
		await().atMost(5, TimeUnit.SECONDS).until("#"+CommonObjects.FILTER_BTN_ID).areDisplayed();
		click("#"+CommonObjects.FILTER_BTN_ID);
		CommonUtil1.Sync();
	
	}
	
	
	public void openApplication() throws IOException, InterruptedException {

		String username,password;
        if(PropertyManager.getProperty("use.sauce.labs").equalsIgnoreCase("yes"))
            setDriver(createDriver());
        else{
            //Initialize the webdriver
    	String ieDriverServer = PropertyManager.getProperty("driver.server.ie");
        String path = CommonWorkFlowUtil.class.getClassLoader()
                        .getResource(ieDriverServer).getPath();
        System.setProperty("webdriver.ie.driver", path);
        WebDriver driver = new InternetExplorerDriver();
        setDriver(driver);
        }

        driver.manage().window().maximize();

        winHandle = driver.getWindowHandle();
        username= PropertyManager.getProperty("contractmanager.username");
        password = PropertyManager.getProperty("contractmanager.password");
        LoginUtil.logIn(driver, username, password);
        CommonUtil1.Sync();
	}

	public void closeApplication() throws AWTException, IOException{
		
		 try {	
			    initFluent(driver);
			    getDriver().switchTo().window(winHandle);
	            captureScreen();
	            LoginUtil.logOff(getDriver());
	            CommonUtil1.Sync();
	            getDriver().manage().deleteAllCookies();
	            getDriver().close();

	        } finally {
	            getDriver().quit();
	            if(PropertyManager.getProperty("use.sauce.labs").equalsIgnoreCase("yes")){
                     // Process destroy is not working, killing the SauceConnect process
                     Runtime.getRuntime().exec("taskkill /f /im sc.exe") ;
//                     System.out.println("Executed kill process!!!!!");
                 }
	     }
	}
	
	public void createCategory(String category1) throws AWTException{
		
		initFluent(driver);
		CommonUtil.clickIfElementIdPresent(getDriver(),
				AdminObjects.MAIN_CREATE_BTN_ID);
		CommonUtil1.Sync();
		CommonUtil.typeIfElementIdPresent(getDriver(), AdminObjects.NAME_INPUT_ID,
				category1);
		CommonUtil.typeIfElementIdPresent(getDriver(), AdminObjects.DESC_INPUT_ID,
				"Test Category");
		CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
		CommonUtil1.Sync();
		performSearch("Name",category1);
		
	}
	
	public void performBrowse(String BrowseDropDownValue,String BrowsingBy){
		
		initFluent(driver);
		await().atMost(5000).until("#BrowseDropDown").isPresent();
		find("select",withName("BrowseDropDown")).find("option", withText(BrowseDropDownValue)).click();
		sleepThread(500);
		CommonUtil1.Sync();
		await().atMost(5, TimeUnit.SECONDS).until("table.browseSelection").isPresent();
		find("td", with("class", "browseInactiveTD"), withText(BrowsingBy)).click();
		CommonUtil1.Sync();
	}
	
	public void applyDelete(){
		
		String parentWindow =null;
		initFluent(driver);
		parentWindow = getDriver().getWindowHandle();
		click("#btnDelete");
		CommonUtil1.alertAcceptOrDismiss("accept");
		await().atMost(5000).until("#progressBarTitle").isPresent();
		sleepThread(2000);
		await().atMost(10000).until("#progressBarTitle").isNotPresent();
		getDriver().switchTo().window(parentWindow);
	}
	
	public void createCustomForm(String FormName,String FormType,String FieldVal1,String FieldVal2,String FieldVal3){
		
		initFluent(driver);
		WebDriver popUpWindow = null;
    	String parentWindow=null;
		click("#btnMainCreate");
        CommonUtil1.Sync();
        await().atMost(5, TimeUnit.SECONDS).until("td.contentTitle").withText().contains("Name").isPresent();
        fill("#fieldname").with(FormName);
        find("#formType").find("option", withText(FormType)).click();
        fill("#description").with(FormName);
        click("#global");
        click("#disabled");
        find("td.wizardStepTDInactive", withText().contains("Contents")).click();
        await().atMost(5, TimeUnit.SECONDS).until("td.contentTitle").withText().contains("Contents").isPresent();
        addFieldToForm(FieldVal1);
        await().atMost(5, TimeUnit.SECONDS).until("input").withName("selectionids").hasSize(1);
        addFieldToForm(FieldVal2);
        await().atMost(5, TimeUnit.SECONDS).until("input").withName("selectionids").hasSize(2);
        addFieldToForm(FieldVal3);
        await().atMost(5, TimeUnit.SECONDS).until("input").withName("selectionids").hasSize(3);
        for(FluentWebElement element: find("input", withName("required"))){
            element.click();
        }
        click("#btnSave");
        CommonUtil1.Sync();
        assertThat(find("span.detailsLink", withText().contains(FormName))).isNotEmpty();
        parentWindow = getDriver().getWindowHandle();
        find("span.detailsLink", withText().contains(FormName)).click();
        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CustomFormView");  
        assertThat(find("td.formViewData").first().getText()).isEqualTo(FormName);
        assertThat(find("td.formViewData", 1).getText()).isEqualTo(FormType);
        assertThat(find("td.formViewData", 3).getText()).isEqualTo(FormName);
        click("#fields");
        assertThat(find("span.detailsLink", 0).getText()).contains(FieldVal1);
        assertThat(find("span.detailsLink", 1).getText()).contains(FieldVal2);
        assertThat(find("span.detailsLink", 2).getText()).contains(FieldVal3);
        click("#btnDialogClose");
        getDriver().switchTo().window(parentWindow);
	}
	
	public void addFieldToForm(String fieldName) {
		
		initFluent(driver);
		WebDriver popUpWindow = null;
    	String parentWindow=null;
    	parentWindow = getDriver().getWindowHandle();
        click("#btnAddFormField");
        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractFieldChooser");  
        performSearch("Name",fieldName);
        click("input", withName("selectionids"));
        click("#btnChooseSelected");
        getDriver().switchTo().window(parentWindow);
        sleepThread(1000);
    }
	
	public String findingId(String FormName) {
		
		String FormId=null;
		initFluent(driver);
		WebDriver popUpWindow = null;
    	String parentWindow=null;
        assertThat(find(".detailsLink", withText().contains(FormName))).isNotEmpty();
        parentWindow = getDriver().getWindowHandle();
        find("span.detailsLink", withText().contains(FormName)).click();
        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CustomFormView"); 
        FormId = url().substring(url().length() - 36);
        popUpWindow.close();
        getDriver().switchTo().window(parentWindow);
		return FormId;
    }
	
	 public int findingIndexByName(String name) {
		    initFluent(driver);
	        sleepThread(1000);
	        return find("td", with("columnid").contains("name")).getTexts().indexOf(name);
	 }
	 
	 
	 public void requestReview(String SearchBy,String SearchFor){
		 
		initFluent(driver);
		WebDriver popUpWindow = null;
	    String parentWindow=null;
	    performSearch(SearchBy,SearchFor);
        assertThat(find("span", withText().contains(SearchFor))).isNotEmpty();
        findFirst("input",withName().equalTo("selectionids")).click();
		click("#btnRequestReview");
		CommonUtil1.Sync();
        fill("input", withName("DueDate")).with("12/12/9999");
        find("td.wizardStepTDInactive", withText().contains("Reviewers")).click();
        CommonUtil1.Sync();
       /* parentWindow = getDriver().getWindowHandle();
        find("span",withId().contains("AddReviewer")).click();
        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser");  
        performSearch("Username","reviewer");
        findFirst("input",withName().equalTo("selectionids")).click();
        click("#btnChooseSelected");
        getDriver().switchTo().window(parentWindow);*/
        await().atMost(5000).until("#btnSave").isPresent();
        click("#btnSave");
        CommonUtil1.Sync();
	 }
	 
	 public void cancelReview(String SearchBy,String SearchFor) throws AWTException{
		 	
		    initFluent(driver);
		    WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Review/Approval")));
		  	findFirst("a",withText("Review Requests")).click();
		  	CommonUtil1.Sync();
	        performSearch(SearchBy,SearchFor);
	        findFirst("input",withName().equalTo("selectionids")).click();
	        parentWindow = getDriver().getWindowHandle();
	        click("#btnCancelReview");
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CloseReview"); 
	        find("#messagetext",0).text("Cancelling the review");
	        click("#btnOKbk");
	        getDriver().switchTo().window(parentWindow);
	 }
	 
	 public void sendReminder(String SearchBy,String SearchFor) throws AWTException{
		 	
		    initFluent(driver);
		    WebDriver popUpWindow = null;
	    	String parentWindow=null;
	        performSearch(SearchBy,SearchFor);
	        findFirst("input",withName().equalTo("selectionids")).click();
	        parentWindow = getDriver().getWindowHandle();
	        click("#btnSendReminder");
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Reminder");
	        await().atMost(5000).until("#messagetext").isPresent();
	        findFirst("#messagetext").text("A gentle reminder");
	        click("#btnOKbk");
	        getDriver().switchTo().window(parentWindow);
	 }
	 
	 public void reviewTask(String SearchBy,String SearchFor) throws AWTException{
		 
		 	initFluent(driver);
		    WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Review/Approval")));
		  	findFirst("a",withText("Review Tasks")).click();
		  	CommonUtil1.Sync();
	        performSearch(SearchBy,SearchFor);
	        findFirst("input",withName().equalTo("selectionids")).click();
	        click("#btnReview");
	        CommonUtil1.Sync();
	        sleepThread(3000);
	        parentWindow = getDriver().getWindowHandle();
	        find("#btnSaveAndComplete").click();
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CompleteComments");
	        fill("#messagetext").with("Review done");
	        click("#btnOKbk");
	        getDriver().switchTo().window(parentWindow);
	 }
	 
	 public void closeReview(String SearchBy,String SearchFor) throws AWTException{
		 
		 	initFluent(driver);
		 	WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Review/Approval")));
		  	findFirst("a",withText("Review Requests")).click();
		  	CommonUtil1.Sync();
	        performSearch(SearchBy,SearchFor);
	        assertThat(find("input[statusstr='Reviewed No Changes']")).isNotEmpty();
	        findFirst("input",withName().equalTo("selectionids")).click();
	        parentWindow = getDriver().getWindowHandle();
	        click("#btnCloseReview");
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CloseReview");
	        fill("#messagetext").with("Closing Review");
	        click("#btnOKbk");
	        getDriver().switchTo().window(parentWindow);
	        
	 }
	 
	 public String suggestChange(String SearchBy,String SearchFor) throws AWTException{
	    	
		    initFluent(driver);
		    WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	performSearch(SearchBy,SearchFor);
	        findFirst("input",withName().equalTo("selectionids")).click();
	        find("span.button.context",withText().contains("Open")).click();
	        CommonUtil1.Sync();
	        sleepThread(3000);
	        findFirst("li",withId().contains("termListItem")).click();
	        findFirst("li",withId().contains("termListItem")).click();
//	        find("span",withText().contains(SearchFor)).click();
	        sleepThread(1000);
	        click("#btnSuggestChange");
	        await().atMost(5000).until("#word-0").isPresent();
	        findFirst("#word-0").doubleClick();
	        sleepThread(1000);
	        SearchFor=SearchFor+"newWord";
	        find("#newWord").text(SearchFor);
	        click("#btnWordDialogOk");
	        click("#btnSuggestedChangeEditorOk");
	        CommonUtil1.Sync();
	        sleepThread(3000);
	        parentWindow = getDriver().getWindowHandle();
	        findFirst("span",withText().contains("Save & ")).click();
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Comments");
	        fill("#messagetext").with("Done");
	        click("#btnOKbk");
	        getDriver().switchTo().window(parentWindow);
			return SearchFor;
	    	
	    	
	  }
	 
	 public void acceptChange(String SearchBy,String SearchFor) throws AWTException{
	    	
		    initFluent(driver);
		    WebDriver popUpWindow1 = null,popUpWindow2 = null;
	    	String parentWindow1=null,parentWindow2=null;
		    performSearch(SearchBy,SearchFor);
	        parentWindow1=getDriver().getWindowHandle();
	        find("span",withText().contains(SearchFor)).click();
	        popUpWindow1=CommonUtil1.navigateToWindowBasedOnURL("TermsView");
	        find("td",withId().contains("review/Approval")).click();
	        await().atMost(5000).until("span.detailsLink").isPresent();
	        parentWindow2=getDriver().getWindowHandle();
	        find("div.contentListDiv").find("tr",withText().contains("Pending Requested Changes")).find("span.detailsLink").click();
	        popUpWindow2=CommonUtil1.navigateToWindowBasedOnURL("reviews,Review");
	        find("span",withText().contains(SearchFor)).click();
	        await().atMost(5000).until("div.comment.change.editable").isPresent();
	        find("div.comment.change.editable",0).click();
	        await().atMost(5000).until("#btnAcceptSuggestedChange").isPresent();
	        click("#btnAcceptSuggestedChange");
	        CommonUtil1.alertAcceptOrDismiss("accept");
	        CommonUtil1.Sync();
	        click("#btnSave");
	        getDriver().switchTo().window(parentWindow2);
	        popUpWindow1.close();
	        getDriver().switchTo().window(parentWindow1);
	  }
	 
	 
	 public String performTermMinorEdit(String SearchBy,String SearchFor) throws AWTException{
	    	
		    initFluent(driver);
		    WebDriver popUpWindow1 = null,popUpWindow2 = null;
	    	String parentWindow1=null,parentWindow2=null,newWord=null;
	    	performSearch(SearchBy,SearchFor);
		  	parentWindow1=getDriver().getWindowHandle();
	        find("span",withText().contains(SearchFor)).click();
	        popUpWindow1=CommonUtil1.navigateToWindowBasedOnURL("TermsView");
	        find("td",withId().contains("review/Approval")).click();
	        await().atMost(5000).until("span.detailsLink").isPresent();
	        parentWindow2=getDriver().getWindowHandle();
	        find("div.contentListDiv").find("tr",withText().contains("Pending Requested Changes")).find("span[title='View Details']").click();
	        popUpWindow2=CommonUtil1.navigateToWindowBasedOnURL("reviews,Review");
	        find("span",withText().contains(SearchFor)).click();
	        await().atMost(5000).until("#btnEditTerm").isPresent();
	        sleepThread(3000);
		  	findFirst("span",withId().contains("btnEditTerm")).click();
	        await().atMost(5000).until("#word-0").isPresent();
	        findFirst("#word-0").doubleClick();
	        sleepThread(1000);
	        SearchFor=SearchFor+"newWord";
	        find("#newWord").text(SearchFor);
	        click("#btnWordDialogOk");
	        CommonUtil1.Sync();
	        await().atMost(5000).until("#btnTermEditorOk").isPresent();
	        click("#btnTermEditorOk");
	        CommonUtil1.Sync();
	        sleepThread(3000);
	        click("#btnSave");
	        getDriver().switchTo().window(parentWindow2);
	        popUpWindow1.close();
	        getDriver().switchTo().window(parentWindow1);
	        
			return SearchFor;
	    	
	  }
	 
	 public void requestApproval(String SearchBy,String SearchFor){
		 
			initFluent(driver);
			WebDriver popUpWindow = null;
		    String parentWindow=null;
		    performSearch(SearchBy,SearchFor);
	        assertThat(find("span", withText().contains(SearchFor))).isNotEmpty();
	        findFirst("input",withName().equalTo("selectionids")).click();
			click("#btnRequestApproval");
			CommonUtil1.Sync();
	        fill("input", withName("DueDate")).with("12/12/9999");
	        find("td.wizardStepTDInactive", withText().contains("Approvers")).click();
	        parentWindow = getDriver().getWindowHandle();
	        find("span",withId().contains("AddApprover")).click();
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser");  
	        performSearch("Username","approver");
	        findFirst("input",withName().equalTo("selectionids")).click();
	        click("#btnChooseSelected");
	        getDriver().switchTo().window(parentWindow);
	        await().atMost(5000).until("#btnSave").isPresent();
	        click("#btnSave");
	        CommonUtil1.Sync();
	 }
	 
	 public void approvalTask(String SearchBy,String SearchFor) throws AWTException{
		 
		 	initFluent(driver);
		    WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Review/Approval")));
		  	findFirst("a",withText("Approval Tasks")).click();
		  	CommonUtil1.Sync();
	        performSearch(SearchBy,SearchFor);
	        findFirst("input",withName().equalTo("selectionids")).click();
	        click("#btnCompleteApproval");
	        CommonUtil1.Sync();
	        sleepThread(5000);
	        parentWindow = getDefaultDriver().getWindowHandle();
	        click("#btnSaveAndApprove");
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ApprovalComments"); 
	        fill("#messagetext").with("Approved");
	        click("#btnOKbk");
	        getDefaultDriver().switchTo().window(parentWindow);
	 }
	 
	 public void closeApproval(String SearchBy,String SearchFor) throws AWTException{
		 
		 	initFluent(driver);
		 	WebDriver popUpWindow = null;
	    	String parentWindow=null;
	    	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Review/Approval")));
		  	findFirst("a",withText("Approval Requests")).click();
		  	CommonUtil1.Sync();
	        performSearch(SearchBy,SearchFor);
	        assertThat(find("input[statusstr='Pending Requested Changes']")).isNotEmpty();
	        findFirst("input",withName().equalTo("selectionids")).click();
	        parentWindow = getDriver().getWindowHandle();
	        click("#btnCloseApproval");
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CloseApproval"); 
	        fill("#messagetext").with("Closing Approval");
	        click("#btnOKbk");
	        getDefaultDriver().switchTo().window(parentWindow);
	        
	 }
	 
	 public void createTerm(String TermName) throws AWTException{
		 
		 initFluent(driver);
		 CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Language")));
		 findFirst("a",withText("Terms")).click();
		 CommonUtil1.Sync();
		 click("#btnCreateTerm");
		 CommonUtil1.Sync();
	     fill("#termname").with(TermName);
	     click("#btnSave");
	     CommonUtil1.Sync();
		 
	 }
	 
	 public void createCustomField(String FieldName,String FieldLabel,String FieldType){
		 
		initFluent(driver);
		click("#btnMainCreate");
		CommonUtil1.Sync();
	    fill("#fieldname").with(FieldName);
        fill("#fieldlabel").with(FieldLabel);
        find("select", withName("fieldtype")).find("option", withText(FieldType)).click();
        sleepThread(1000);
        if(!find("span.detailsLink").find("i", withText("Add option")).isEmpty()){
        	find("span.detailsLink").find("i", withText("Add option")).click();
            fill("#optionRow0").with("Option 1");
            fill("#optionRow2").with("Option 2");
        }
        if(FieldType.equalsIgnoreCase("Link")){
        	fill("#defaultvalue").with(getDefaultBaseUrl() + "/template/admin,CustomFieldView.vm?id=");
            fill("#parameterRow0").with("test");
        }
        click("#btnSave");
        CommonUtil1.Sync();
	 }
	 
	 public void createDocument(String DocName,String FileName) throws AWTException {
		 	
		    WebDriver popUpWindow = null;
	    	String parentWindow=null,filePath=null;
	    	initFluent(driver);
	        CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Language")));
		  	findFirst("a",withText("Documents")).click();
		  	CommonUtil1.Sync();
	        CommonUtil.clickIfElementIdPresent(getDriver(), LangDocAndTermsObjects.CREATE_DOCUMENT_BTN_ID);
	        CommonUtil1.Sync();
	        parentWindow = getDriver().getWindowHandle();
	        CommonUtil.clickIfElementIdPresent(getDriver(), LangDocAndTermsObjects.DOCUMENT_CHOOSER_BTN_ID);
	        popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ImportUploader");
	        filePath= CommonUtil.getFilePath(FileName);
	        filePath = filePath.substring(1).replace("/", "\\");
	        CommonUtil.typeIfFileElementIdPresent(getDriver(), CommonObjects.FILE_ID, filePath);
	        CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.OKBK_BTN_ID);
	        getDriver().switchTo().window(parentWindow);
	        CommonUtil.clickIfElementIdPresent(getDriver(), LangDocAndTermsObjects.IMPORT_DOC_BTN_ID);
	        CommonUtil.typeIfElementIdPresent(getDriver(), CommonObjects.NAME_TEXT_ID, DocName);
	        CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
	        CommonUtil1.Sync();

	  }
	 
	 public void createContract(String TEMPLATE, String CONTRACT_NAME_1,String BusinessEntity) {
			
		    initFluent(driver);
			String parentWindow = null;
	        WebDriver popUpWindow = null;
	        await().atMost(3, TimeUnit.SECONDS).until("#btnCreateContract")
					.isPresent();
			click("#btnCreateContract");
			CommonUtil1.Sync();
			parentWindow = getDriver().getWindowHandle();
			CommonUtil
					.clickIfElementIdPresent(getDriver(), CommonObjects.CHOOSER_BTN_ID);
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ItemChooser");
			await().atMost(5, TimeUnit.SECONDS).until("select").withName(CommonObjects.SEARCH_DROPDOWN_ID).isPresent();
			performSearch("Name", TEMPLATE);
			CommonUtil.clickIfElementNamePresent(getDriver(),
					CommonObjects.SELECTION_INPUT_NAME);
			CommonUtil.clickIfElementIdPresent(getDriver(),
					CommonObjects.CHOOSE_SELECTED_BTN_ID);
			getDriver().switchTo().window(parentWindow);
			await().atMost(5, TimeUnit.SECONDS).until("td").withText().contains(
					"Details").isPresent();
			find("td", withText().startsWith("Details")).click();
			
			fill("#" + CommonObjects.NAME_TEXT_ID).with(CONTRACT_NAME_1);
			CommonUtil.selectValByEleName(getDriver(),
					ContractsObjects.CONTRACTOR_DROPDOWN_NAME, BusinessEntity);

		}

		public void everGreenRenewalMode() {
			
			initFluent(driver);
			find("td.wizardStepTDInactive",withText().contains("Renewal Mode")).click();
			find("select", withName("renewalmodeid")).find("option",
					withText("Evergreen")).click();
			await().atMost(5000).until("input").withName().contains("EffectiveDate").isPresent();
			fill("input", withName("EffectiveDate")).with("05/05/2013");
			fill("input", withName("ActualEffectiveDate")).with("07/05/2013");
			fill("input", withName("ActualEndDate")).with("01/05/2014");
		}

		public void intialTermWithAutoRenewal() {
			
			initFluent(driver);
			CommonUtil.clickElementByTagAndText(getDriver(), "td", "Renewal Mode");
			CommonUtil.selectValByEleID(getDriver(), ContractsObjects.RENEWAL_MODE_ID,
					"Evergreen");
			find("select", withName("renewalmodeid")).find("option",
					withText("Initial Term with Auto Renewals")).click();
			CommonUtil.typeIfElementNamePresent(getDriver(), "EffectiveDate",
					"05/05/2013");
			// proposedInitEndDate
			fill("input", withName("proposedInitEndDate")).with("07/05/2013");
			fill("input", withName("ActualEffectiveDate")).with("07/05/2013");
			fill("input", withName("ActualEndDate")).with("01/05/2014");
			// notice
			fill("input", withName("notice")).with("1");
			// rolloverPeriod
			fill("input", withName("rolloverPeriod")).with("1");
		}

		public void chooseProvider(String PROVIDER) {
			
			initFluent(driver);
			String parentWindow = null;
	        WebDriver popUpWindow = null;
	        CommonUtil.clickElementByTagAndText(getDriver(), "td", "Providers");
			parentWindow = getDriver().getWindowHandle();
			CommonUtil.clickIfElementIdPresent(getDriver(),
					ContractsObjects.ADD_PROVIDER_BTN_ID);
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ProviderChooser");
			performSearch("Name", PROVIDER);
			CommonUtil.clickIfElementNamePresent(getDriver(),
					CommonObjects.SELECTION_INPUT_NAME);
			CommonUtil.clickIfElementIdPresent(getDriver(),
					CommonObjects.CHOOSE_SELECTED_BTN_ID);
			getDriver().switchTo().window(parentWindow);

		}

		public void AddingCutsomForm() {
			
			initFluent(driver);
			String parentWindow = null;
	        WebDriver popUpWindow = null;
			find("td", withText().startsWith("Custom Forms")).click();
			CommonUtil1.Sync();
			parentWindow = getDriver().getWindowHandle();
			click("#btnAddCustomForm");
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CONTRACT_CUSTOM_FORM");	
			findFirst("input", withName(CommonObjects.SELECTION_INPUT_NAME))
					.click();
			click("#btnChooseSelected");
			getDriver().switchTo().window(parentWindow);
			click(".customforms_link");
			await().atMost(5, TimeUnit.SECONDS).until("input").withId()
					.contains("Test custom field1").isPresent();
			if(!find("input",withId().contains("enable_")).isEmpty()){
				find("input",0,withId().contains("enable_")).click();
				find("input",1,withId().contains("enable_")).click();
			}
			fill("input", with("type", "text"), withId().contains("Test custom field1"))
					.with("CUSTOMFIELD1");
			await().atMost(5, TimeUnit.SECONDS).until("input").withId()
					.contains("Test custom field2").isPresent();
			fill("input", with("type", "text"), withId().contains("Test custom field2"))
					.with("CUSTOMFIELD2");

		}
		
		public void AddingInternalContactsToContract(String user){
			
			initFluent(driver);
			String parentWindow = null;
	        WebDriver popUpWindow = null;
			find("td", withText().startsWith("Internal Contacts")).click();
			parentWindow = getDriver().getWindowHandle();
			click("#btnAddContact");
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("chooserType");
			performSearch("Username",user);
			find("input", with("value").contains(user)).click();
			click("#btnChooseSelected");
			getDriver().switchTo().window(parentWindow);
			
			
		}

		public void saveContract(String CONTRACT_NAME_1) {
			
			initFluent(driver);
			CommonUtil.clickElementByTagAndText(getDriver(), "td", "Guideline");
			CommonUtil.typeIfElementNamePresent(getDriver(), "keywords", "contractstest");
			CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
			CommonUtil1.Sync();
			await().atMost(5000).until("#progressBarTitle").isPresent();
			await().atMost(10000).until("#progressBarTitle").isNotPresent();
			performSearch("Contract Name", CONTRACT_NAME_1);
			assertThat(find("a.detailsLink").getTexts().contains(CONTRACT_NAME_1));
		}

		
		

		public void createInitialTermwithOptionalRenewals() {
			
			initFluent(driver);
			find("td", withText().startsWith("Renewal Mode")).click();
			find("select", withName(ContractsObjects.RENEWAL_MODE_ID)).find(
					"option", withText("Initial Term with Optional Renewals"))
					.click();
			fill("input", withName("EffectiveDate")).with("05/31/2012");
			// proposedInitEndDate
			await().atMost(5, TimeUnit.SECONDS).until("input").withName().contains(
					"notice").isPresent();
			CommonUtil.sleepThread(3000);
			fill("input", withName("notice")).with("1");
			fill("input", withName("proposedInitEndDate")).with("05/31/2013");
			fill("input", withName("rolloverPeriod")).with("1");
		}

		public void createProviderWithSubprovider(String PROVIDER) {
			
			initFluent(driver);
			String parentWindow = null;
	        WebDriver popUpWindow = null;
	        find("td", withText().startsWith("Providers")).click();
			parentWindow = getDriver().getWindowHandle();
			click("#" + ContractsObjects.ADD_PROVIDER_BTN_ID);
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ProviderChooser");	
			performSearch("Name", PROVIDER.trim());
			find("input", withName(CommonObjects.SELECTION_INPUT_NAME)).click();
			click("#" + CommonObjects.CHOOSE_SELECTED_BTN_ID);
			getDriver().switchTo().window(parentWindow);
			find("select", withName("criteriastatus")).find("option",
					withText("Use Criteria")).click();
			CommonUtil.sleepThread(5000);
			await().atMost(10, TimeUnit.SECONDS).until("td.wizardStepTDInactive")
					.withText().contains("SubProvider Criteria").isPresent();
			find("td.wizardStepTDInactive",
					withText().contains("SubProvider Criteria")).click();
			parentWindow = getDriver().getWindowHandle();
			click("#btnAddDirectSubProvider");// SubProviderCriteriaChooser
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("SubProviderCriteriaChooser");	
			await().atMost(10, TimeUnit.SECONDS).until("input").withName()
					.contains("subproviderids").isPresent();
			find("input", withName("subproviderids")).click();
			click("#" + CommonObjects.CHOOSE_SELECTED_BTN_ID);
			getDriver().switchTo().window(parentWindow);

		}

		public void verifyContractForm(String CONTRACT_NAME_1,String Field1,String Field2) {
			
			initFluent(driver);
			String parentWindow = null;
			WebDriver popUpWindow = null;
			find("a", withName(HomePageObjects.CONTRACTS_MENU_LINK_TEXT)).click();
			CommonUtil1.Sync();
			parentWindow = getDriver().getWindowHandle();
			performSearch("Contract Name", CONTRACT_NAME_1);
			findFirst("a.detailsLink", withText(CONTRACT_NAME_1)).click();
			popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractsView");
			click("#custom_Forms");
			assertThat(find("td", withText(Field1 + ":")).contains(
					Field1));
			assertThat(find("td", withText(Field2 + ":")).contains(
					Field2));
			popUpWindow.close();
			getDriver().switchTo().window(parentWindow);

		}
		
public void createMemo(String FILENAME,String MEMOTYPE,String MEMOATTACHMENT ) {
			
		initFluent(driver);
		String parentWindow = null,filePath = null;
        WebDriver popUpWindow = null;
		CommonUtil.clickIfElementNamePresent(getDriver(),
				CommonObjects.SELECTION_INPUT_NAME);
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnMemos");
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnAddMemo");
		parentWindow = getDriver().getWindowHandle();
		CommonUtil
				.clickIfElementIdPresent(getDriver(), CommonObjects.CHOOSER_BTN_ID);
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractComponentChooser");
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnOKbk");
		getDriver().switchTo().window(parentWindow);
		CommonUtil.sleepThread(3000);
		CommonUtil.selectValByEleID(getDriver(), "memoDescription", MEMOTYPE);
		parentWindow = getDriver().getWindowHandle();
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnAttach");
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("AttachmentUploader");
		filePath = CommonUtil.getFilePath(FILENAME + ".xml");
		filePath = filePath.substring(1).replace("/", "\\");
		CommonUtil.typeIfFileElementIdPresent(getDriver(), CommonObjects.FILE_ID,
				filePath);
		CommonUtil.selectValByEleID(getDriver(), "memoAttachmentType",
				MEMOATTACHMENT);
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnOKbk");
		getDriver().switchTo().window(parentWindow);
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnOK");
		CommonUtil.clickIfElementIdPresent(getDriver(), "btnSave");
		CommonUtil1.Sync();

	}

public void creteFixedRenewalMode() {
	
	initFluent(driver);
	CommonUtil.clickElementByTagAndText(getDriver(), "td", "Renewal Mode");
	CommonUtil.selectValByEleID(getDriver(), ContractsObjects.RENEWAL_MODE_ID,
			"Fixed Term");
	CommonUtil.typeIfElementNamePresent(getDriver(), "EffectiveDate",
			"05/05/2013");
	// EndDate
	fill("input", withName("EndDate")).with("05/05/2014");
	
}

public String AddOrRemoveCategories(String SearchFor,String SearchCat,String AddOrRemove,String CategoryValue){
	
	initFluent(driver);
	String parentWindow = null;
	WebDriver popUpWindow = null;
	parentWindow = getDriver().getWindowHandle();
	click("#btnCategorize");
	parentWindow = getDriver().getWindowHandle();
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CategorizePreWizard");
	await().atMost(5, TimeUnit.SECONDS).until("input").withName().contains(
			"radiocategorize").isPresent();
	find("input", with("value").equalTo(AddOrRemove)).click();
	await().atMost(5, TimeUnit.SECONDS).until("#btnOKbk").isPresent();
	click("#btnOKbk");
	getDriver().switchTo().window(parentWindow);
	parentWindow = getDriver().getWindowHandle();
	find("span",withId().endsWith("Category")).click();
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Category");
	if(AddOrRemove.equalsIgnoreCase("categorize")) {
		performSearch(SearchFor,SearchCat);
		findFirst("input",withName(CommonObjects.SELECTION_INPUT_NAME)).click();
		CategoryValue=findFirst("input",withName(CommonObjects.SELECTION_INPUT_NAME)).getValue();
	}else{
		await().atMost(5000).until("input").withName("selectionids").areDisplayed();
		find("input",with("value").equalTo(CategoryValue)).click();
	}
	click("#" + CommonObjects.CHOOSE_SELECTED_BTN_ID);
	getDriver().switchTo().window(parentWindow);
	click("#btnSave");
	CommonUtil1.Sync();
	return CategoryValue;
	
	
}

public void flagComponent(String SearchDropDownValue,String SearchFor){
	
	initFluent(driver);
	WebDriver popUpWindow = null;
	String parentWindow=null;
	performSearch(SearchDropDownValue, SearchFor);
    CommonUtil.clickIfElementNamePresent(getDriver(), CommonObjects.SELECTION_INPUT_NAME);
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.FLAG_BTN_ID);
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.IS_FLAGGED_INPUT_ID);
    parentWindow = getDriver().getWindowHandle();
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.ADD_REVIEWER_BTN_ID);
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser");
    CommonUtil.selectValByEleID(getDriver(), CommonObjects.SEARCH_DROPDOWN_ID, "Username");
    CommonUtil.typeIfElementIdPresent(getDriver(), CommonObjects.SEARCH_TEXT_ID, "reviewer");
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SEARCH_BTN_ID);
    CommonUtil.clickIfElementNamePresent(getDriver(), CommonObjects.SELECTION_INPUT_NAME);
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.CHOOSE_SELECTED_BTN_ID);
    getDriver().switchTo().window(parentWindow);
    parentWindow = getDriver().getWindowHandle();
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.ADD_APPROVER_BTN_ID);
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser");
    CommonUtil.selectValByEleID(getDriver(), CommonObjects.SEARCH_DROPDOWN_ID, "Username");
    CommonUtil.typeIfElementIdPresent(getDriver(), CommonObjects.SEARCH_TEXT_ID, "approver");
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SEARCH_BTN_ID);
    CommonUtil.clickIfElementNamePresent(getDriver(), CommonObjects.SELECTION_INPUT_NAME);
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.CHOOSE_SELECTED_BTN_ID);
    getDriver().switchTo().window(parentWindow);
    CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
    CommonUtil1.Sync();
}

public void createContractTemplate(String TemplateName,String DocumentName) throws AWTException {
	
	initFluent(driver);
	String parentWindow = null;
	WebDriver popUpWindow = null;
	if(!find("a", withText("Contracts")).isEmpty()){
		CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Contracts")));
	  	findFirst("a",withText("Contract Templates")).click();
	  	CommonUtil1.Sync();
		click("#"+ContractsObjects.CREATE_TEMPLATE_BTN_ID);
	}
    fill("#"+CommonObjects.NAME_TEXT_ID).with(TemplateName);
    find("td.wizardStepTDInactive",withText().contains("Contents")).click();
    parentWindow = driver.getWindowHandle();
    click("#btnAddContents");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ItemChooser");
    performSearch("Name",DocumentName);
    findFirst("input",withName(CommonObjects.SELECTION_INPUT_NAME)).click();
    click("#"+CommonObjects.CHOOSE_SELECTED_BTN_ID);
    getDriver().switchTo().window(parentWindow);
    click("#"+CommonObjects.SAVE_BTN_ID);
    CommonUtil1.Sync();
    
}

public void createContentExport(String FILENAME,String DESC,String Template){
	
	initFluent(driver);
	String parentWindow = null;
	WebDriver popUpWindow = null;
	click("#btnCreateDocSet");
	CommonUtil1.Sync();
	fill("#filename").with(FILENAME);
	fill("#description").with(DESC);
	parentWindow = getDriver().getWindowHandle();
	click("#btnAddContract");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ItemChooser");
	performSearch("Name", Template);
	findFirst("input", withName().equalTo("selectionids")).click();
	click("#btnChooseSelected");
	getDriver().switchTo().window(parentWindow);
	click("#btnSave");
	CommonUtil1.Sync();
}

public void createContentImport(String FILENAME,String DESC,String Template){
	
	initFluent(driver);
	String parentWindow = null,filePath = null;
	WebDriver popUpWindow = null;
	click("#btnCreateDocSet");
	CommonUtil1.Sync();
	assertThat(find(".contentTitle").getText().contains("Import"));
	await().atMost(3, TimeUnit.SECONDS).until("#btnChooser").isPresent();
	parentWindow = driver.getWindowHandle();
	click("#btnChooser");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ImportUploader");
	filePath = CommonUtil.getFilePath(FILENAME);
	filePath = filePath.substring(1).replace("/", "\\");
	CommonUtil.typeIfFileElementIdPresent(driver, CommonObjects.FILE_ID,
			filePath);
	click("#btnOKbk");
	getDefaultDriver().switchTo().window(parentWindow);
	fill("#description").with(DESC);
	await().atMost(5, TimeUnit.SECONDS).until(".wizardStepTDInactive")
			.withText().contains("Contents");
	find(".wizardStepTDInactive", withText().contains("Contents")).click();
	assertThat(find("td").getTexts()).contains(Template);
	click("#btnSave");
	CommonUtil1.Sync();
}

public void createAmendment(String CONTRACT_NAME,String TEST_TEMPLATE,String AMENDMENT_NAME,String AMENDMENT_LABEL,String ROOT_USER,String MEMO_TEXT,String GUIDELINE_TEXT,String KEYWORD,String CATEGORY){
	
	initFluent(driver);
	WebDriver popUpWindow = null;
	String parentWindow=null;
	click("#btnCreateAmendments");
	CommonUtil1.Sync();
	parentWindow = getDriver().getWindowHandle();
	click("#btnAddContract");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser"); 
	performSearch("Contract Name",CONTRACT_NAME);
	clickIfElementNamePresent(getDriver(), "selectionids");
	click("#btnChooseSelected");
	getDriver().switchTo().window(parentWindow);
	await().atMost(5000).until("#btnSave").isPresent();
	click("#btnSave");
	CommonUtil.sleepThread(5000);
	CommonUtil1.Sync();
	await().atMost(5000).until("#btnChooser").isPresent();
	parentWindow = getDriver().getWindowHandle();
	click("#btnChooser");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ItemChooser"); 
	performSearch("Name",TEST_TEMPLATE);
	clickIfElementNamePresent(getDriver(), "selectionids");
	click("#btnChooseSelected");
	getDriver().switchTo().window(parentWindow);
	await().atMost(5000).until("#btnSave").isPresent();
	find("td.wizardStepTDInactive", withText().contains("Details")).click();
	if(AMENDMENT_NAME!=null) fill("#amendment_name").with(AMENDMENT_NAME);
	fill("#label").with(AMENDMENT_LABEL);
	fill("input", withName().startsWith("EffectiveDate"))
			.with("01/01/2020");
	find("td.wizardStepTDInactive", withText().contains("Renewal Mode"))
			.click();
	CommonUtil1.Sync();
	
	if(new Select(getDriver().findElement(By.name("renewalmodeid"))).getAllSelectedOptions().get(0).getText().contains("Initial Term with")){
		
		fill("input", withName().startsWith("proposedEffectiveDate")).with("01/01/2020");
		fill("input", withName().startsWith("proposedInitEndDate")).with(
				"01/01/9999");
		fill("input", withName().startsWith("notice")).with("50");
		fill("input", withName().startsWith("rolloverPeriod")).with("30");
	}
	
	if(new Select(getDriver().findElement(By.name("renewalmodeid"))).getAllSelectedOptions().get(0).getText().equalsIgnoreCase("Evergreen")){
	
		fill("input", withName().startsWith("proposedEffectiveDate")).with(
				"01/01/2020");
		fill("input", withName().startsWith("ActualEffectiveDate")).with(
				"01/02/2020");
		fill("input", withName().startsWith("ActualEndDate"))
				.with("01/01/9999");
	}
	
	if(new Select(getDriver().findElement(By.name("renewalmodeid"))).getAllSelectedOptions().get(0).getText().equalsIgnoreCase("Fixed Term")){
		
		fill("input", withName().startsWith("proposedEffectiveDate")).with(
				"01/01/2020");
		fill("input", withName().startsWith("proposedEndDate")).with(
				"01/01/9999");
		fill("input", withName().startsWith("ActualEffectiveDate")).with(
				"01/02/2020");
		fill("input", withName().startsWith("ActualEndDate"))
				.with("01/01/9999");
	}
		
	find("td.wizardStepTDInactive",
			withText().contains("Internal Contacts")).click();
	CommonUtil1.Sync();
	parentWindow = getDriver().getWindowHandle();
	click("#btnAddContact");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser"); 
	performSearch("Username",ROOT_USER);
	clickIfElementNamePresent(getDriver(), "selectionids");
	click("#btnChooseSelected");
	getDriver().switchTo().window(parentWindow);
	find("td.wizardStepTDInactive", withText().contains("Memos")).click();
	click("#btnAddMemo");
	CommonUtil1.Sync();
	await().atMost(5, TimeUnit.SECONDS).until("#memotext").isPresent();
	fill("#memotext").with(MEMO_TEXT);
	click("td.wizardStepTDInactive", withText().contains("Guideline"));
	await().atMost(3, TimeUnit.SECONDS).until("#guideline").areDisplayed();
	fill("#guideline").with(GUIDELINE_TEXT);
	fill("textarea", withName().contains("keywords")).with(KEYWORD);
	find("td.wizardStepTDInactive", withText().contains("Categories"))
			.click();
	parentWindow = getDriver().getWindowHandle();
	click("#btnAddCategory");
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CategoryChooser"); 	
	performSearch("Name",CATEGORY);
	clickIfElementNamePresent(getDriver(), "selectionids");
	click("#btnChooseSelected");
	getDriver().switchTo().window(parentWindow);
	click("#btnSave");
	CommonUtil1.Sync();
}

public void bulkBypassReview(String SearchBy,String SearchFor){
	
	initFluent(driver);
	String parentWindow = null;
	WebDriver popUpWindow = null;
	await().atMost(5, TimeUnit.SECONDS).until("td.contentButtons")
			.withText().contains("Bulk Bypass Review").isPresent();
	click("#btnBulkBypassReview");
	CommonUtil1.Sync();
	parentWindow = getDriver().getWindowHandle();
	CommonUtil.clickIfElementIdPresent(getDriver(),
			ContractsObjects.ADD_CONTRACT_BTN_ID);
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser");
	performSearch(SearchBy, SearchFor);
	CommonUtil.clickIfElementIdPresent(getDriver(),
			ContractsObjects.CHOOSE_ALL_BTN_ID);
	getDriver().switchTo().window(parentWindow);
	CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
	CommonUtil1.Sync();
}

public void statusChange(String SearchBy,String SearchFor,String Status){
	
	initFluent(driver);
	performSearch(SearchBy,SearchFor);
	clickIfElementNamePresent(getDriver(), "selectionids");
	click("#btnStatus");
	CommonUtil1.Sync();
	find("#contractstatus").find("option", withText().contains(Status)).click();
	click("#btnSave");
	CommonUtil1.Sync();
}

public void createQuery(String QueryName,String QueryDescription,String QueryBasedOn,String KeyName,String Comparator,String KeyValue){
	 
	 initFluent(driver);
	 click("#btnCreateQuery");
	 CommonUtil1.Sync();
     fill("#queryname").with(QueryName);
     fill("#description").with(QueryDescription);
     if(QueryBasedOn!=null) find("select",withName("useInitialMetaData")).find("option", withText(QueryBasedOn)).click();
     find("#keyname_0").find("option", withText(KeyName)).click();
     find("#comparator_0").find("option", withText(KeyValue)).click();
     fill("#keyvalue_0").with(KeyValue);
     click("#btnSave");
     CommonUtil1.Sync();
}

public void runQuery(String Search,String SearchFor,String QueryTarget){
	
	initFluent(driver);
	WebDriver popUpWindow = null;
	String parentWindow=null;
    performSearch(Search,SearchFor);
    clickIfElementNamePresent(getDriver(), "selectionids");
    parentWindow = getDriver().getWindowHandle();
    click("#btnRunQuery");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("QueryTargetChooser"); 
    find("#target").find("option", withText().contains(QueryTarget)).click();
    click("#btnOKbk");
    getDriver().switchTo().window(parentWindow);
    CommonUtil1.Sync();
}

public void bulkPrint(String Search,String SearchFor,String Filter,String FilterFor) throws AWTException{
	
	initFluent(driver);
	String parentWindow = null,parentWindow1 = null;
	WebDriver popUpWindow = null,popUpWindow1 = null;
	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Tools")));
  	find("a",withText("Create Print File")).click();
  	CommonUtil1.Sync();
	parentWindow = getDriver().getWindowHandle();
	CommonUtil.clickIfElementIdPresent(getDriver(),
			CommonObjects.MAIN_CREATE_BTN_ID);
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Create_Pdfs");
	await().atMost(5000).until("input[value='pdf']").isPresent();
	findFirst("#pdf").click();
	find("td.formcontentData").findFirst("input").click();
	parentWindow1 = getDriver().getWindowHandle();
	CommonUtil.clickIfElementIdPresent(getDriver(),
			ContractsObjects.ADD_CONTRACT_BTN_ID);
	popUpWindow1=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser");
	performSearch(Search, SearchFor);
	applyFilter(Filter, FilterFor);
	click("#btnChooseAll");
	getDriver().switchTo().window(parentWindow1);
	CommonUtil.clickIfElementIdPresent(getDriver(), CommonObjects.SAVE_BTN_ID);
	CommonUtil1.Sync();
	await().atMost(60000).until("div").withText().contains("Generation successfully completed!").areDisplayed();
	assertThat(find("div",withText().contains("Generation successfully completed!")).isEmpty()).isFalse();
	popUpWindow1.close();
	getDriver().switchTo().window(parentWindow);
}

public void bulkStatusChange(String Type,String Search,String SearchFor,String Filter,String FilterValue){
	
	initFluent(driver);
	String parentWindow = null;
	WebDriver popUpWindow = null;
	click("#btnBulkStatusChange");
	CommonUtil1.Sync();
	parentWindow = getDriver().getWindowHandle();
	click("#btnAdd"+Type);
	popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser");
	performSearch(Search,SearchFor);
	applyFilter(Filter,FilterValue);
	click("#btnChooseAll");
	getDriver().switchTo().window(parentWindow);
	click("#btnSave");
	CommonUtil1.Sync();
}

public void createAffiliatedContract(String BASE_CONTRACT,String TEST_TEMPLATE,String AFFIL_NAME,String AFFIL_TYPE,String RenewalMode,String User,String MEMO_TEXT,String GUIDELINE_TEXT,String KEYWORD,String CATEGORY) throws AWTException{
	
	initFluent(driver);
	WebDriver popUpWindow = null;
	String parentWindow=null;
	CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Contracts")));
  	find("a",1,withText("Contracts")).click();
  	CommonUtil1.Sync();
    click("#btnCreateAffiliations");
    assertThat(find("span.wizardTitle", withText().contains("Select Base Contracts"))).isNotEmpty();
    parentWindow = getDriver().getWindowHandle();
    click("#btnAddContract");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser");
    performSearch("Contract Name",BASE_CONTRACT);
    clickIfElementNamePresent(getDriver(), "selectionids");
    click("#btnChooseSelected");
    getDriver().switchTo().window(parentWindow);
    await().atMost(5, TimeUnit.SECONDS).until("#btnSave").isPresent();
    click("#btnSave");
    CommonUtil1.Sync();
    await().atMost(5, TimeUnit.SECONDS).until("#btnChooser").isPresent();
    parentWindow = getDriver().getWindowHandle();
    click("#btnChooser");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ItemChooser");
    performSearch("Name",TEST_TEMPLATE);
    clickIfElementNamePresent(getDriver(), "selectionids");
    click("#btnChooseSelected");
    getDriver().switchTo().window(parentWindow);
    find("td.wizardStepTDInactive", withText().contains("Details")).click();
    CommonUtil1.Sync();
    if(AFFIL_NAME!=null) fill("#name").with(AFFIL_NAME);
    find("select", withName().contains("typeid")).find("option", withText().contains(AFFIL_TYPE)).click();
    find("td.wizardStepTDInactive", withText().contains("Renewal Mode")).click();
    CommonUtil1.Sync();
    find("#renewalmodeid").find("option", withText().contains(RenewalMode)).click();
    if(RenewalMode.contains("Initial Term with")){
    	fill("input", withName().startsWith("EffectiveDate")).with(startDate);
		fill("input", withName().startsWith("proposedInitEndDate")).with(endDate);
		fill("input", withName().startsWith("actualInitEndDate")).with(endDate);
		fill("input", withName().startsWith("notice")).with("50");
		fill("input", withName().startsWith("rolloverPeriod")).with("30");
	}
	
	if(RenewalMode.equalsIgnoreCase("Evergreen")){
		fill("input", withName().startsWith("EffectiveDate")).with(startDate);
		fill("input", withName().startsWith("ActualEffectiveDate")).with(startDate);
		fill("input", withName().startsWith("ActualEndDate")).with(endDate);
	}
	
	if(RenewalMode.equalsIgnoreCase("Fixed Term")){
		fill("input", withName().startsWith("EffectiveDate")).with(startDate);
		fill("input", withName().startsWith("EndDate")).with(endDate);
		fill("input", withName().startsWith("ActualEffectiveDate")).with(startDate);
		fill("input", withName().startsWith("ActualEndDate")).with(endDate);
	}
	find("td.wizardStepTDInactive", withText().contains("Internal Contacts")).click();
    CommonUtil1.Sync();
    parentWindow = getDriver().getWindowHandle();
    click("#btnAddContact");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("UserChooser");
    performSearch("Username",User);
    clickIfElementNamePresent(getDriver(), "selectionids");
    click("#btnChooseSelected");
    getDriver().switchTo().window(parentWindow);
    find("td.wizardStepTDInactive", withText().contains("Memos")).click();
    CommonUtil1.Sync();
    click("#btnAddMemo");
    fill("#memotext").with(MEMO_TEXT);
    click("td.wizardStepTDInactive", withText().contains("Guideline"));
    CommonUtil1.Sync();
    await().atMost(3, TimeUnit.SECONDS).until("#guideline").areDisplayed();
    fill("#guideline").with(GUIDELINE_TEXT);
    fill("textarea", withName().contains("keywords")).with(KEYWORD);
    find("td.wizardStepTDInactive", withText().contains("Categories")).click();
    CommonUtil1.Sync();
    parentWindow = getDriver().getWindowHandle();
    click("#btnAddCategory");
    popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("CategoryChooser");
    performSearch("Name",CATEGORY);
    clickIfElementNamePresent(getDriver(), "selectionids");
    click("#btnChooseSelected");
    getDriver().switchTo().window(parentWindow);
    click("#btnSave");
    CommonUtil1.Sync();
}

public String getAppDateFormat(String Date){
	
	String expectedString=null;
	String[] Splitted_Date = null;
	try{
		Splitted_Date = Date.trim().split("/");
		switch(Integer.parseInt(Splitted_Date[0])){
		case 1:expectedString="January "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 2:expectedString="February "+Splitted_Date[1]+", "+Splitted_Date[2];
	    break;
		case 3:expectedString="March "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 4:expectedString="April "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 5:expectedString="May "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 6:expectedString="June "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 7:expectedString="July "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 8:expectedString="August "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 9:expectedString="September "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 10:expectedString="October "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 11:expectedString="November "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		case 12:expectedString="December "+Splitted_Date[1]+", "+Splitted_Date[2];
		break;
		default :expectedString="Invalid Month or format. Please enter date in MM/DD/YYYY format only!";
		}
	}catch(Exception e){
		System.out.println("Invalid Month or format. Please enter date in MM/DD/YYYY format only!");
	}
	System.out.println("Expected App Date Format! "+expectedString);
	
	return expectedString;
}

public void generatePDF(String Search,String SearchFor,String expPDFText){
	
	initFluent(driver);
	String parentWindow = null,parentWindow1 = null,actPDFText=null;
    WebDriver popUpWindow = null,popUpWindow1 = null;
    String[] splittedExpPDFText=null;
    boolean containsPDFText=true;
    int count=0;
	performSearch(Search,SearchFor);
	parentWindow = getDriver().getWindowHandle(); 
	find("span",withText().contains(SearchFor.trim())).click();
	popUpWindow =  CommonUtil1.navigateToWindowBasedOnURL("View.vm");
	splittedExpPDFText=expPDFText.trim().split("\\s");
	parentWindow1 = getDriver().getWindowHandle(); 
	find("a",withText().contains("Generate PDF")).click();
	popUpWindow1 =  CommonUtil1.navigateToWindowBasedOnURL("DownloadPDFRequest.vm");
	CommonUtil1.Sync();
	await().atMost(10000).until("p").withText().contains("Generating PDF, Please Wait").areDisplayed();
	while(getDriver().getCurrentUrl().contains("DownloadPDFRequest.vm")){
		sleepThread(1000);
		if(count==60) break;
		count=count+1;
	}
	assertThat(getDriver().getCurrentUrl().contains("DownloadPDF.vm")).isTrue();
	actPDFText=getDriver().findElement(By.tagName("body")).getText();
	for(int i=0;i<splittedExpPDFText.length;i++){
		if(!actPDFText.contains(splittedExpPDFText[i])){
			containsPDFText=false;
		}
	}
	assertThat(containsPDFText).isTrue();
	popUpWindow1.close();
	getDriver().switchTo().window(parentWindow1);
	popUpWindow.close();
	getDriver().switchTo().window(parentWindow);
}

    public WebDriver createDriver() throws MalformedURLException {
/*
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String str = EnvironmentUtil.getCapabilities();
        logger.info(str);
        try {
            JSONArray jsonArray = new JSONArray(str);
            int i = jsonArray.length();
            logger.info("JSONArray Length is :"+i);

            for (int j = 0; j <= i; j++) {
                JSONObject object = jsonArray.getJSONObject(j);
                logger.info("JSONObject "+j+ " is:"+ object);
                capabilities = setCapabilities(object);
                capabilities.setCapability("name", "Set "+j);

                String sauceUser = (String) capabilities.getCapability("sauceUser");
                String sauceKey = (String) capabilities.getCapability("sauceKey");

                driver = new RemoteWebDriver(new URL("http://" + sauceUser + ":"
                        + sauceKey + "@ondemand.saucelabs.com:8"+j+"/wd/hub"),
                        capabilities);

                logger.info("SauceOnDemandSessionID="+ (((RemoteWebDriver) driver).getSessionId()).toString() + " job-name=Job Test");

                //return driver;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
*/
        DesiredCapabilities capabilities = new DesiredCapabilities();
        EnvironmentUtil.setCapabilities(capabilities);
        String sauceUser = (String) capabilities.getCapability("sauceUser");
        String sauceKey = (String) capabilities.getCapability("sauceKey");

        driver = new RemoteWebDriver(new URL("http://" + sauceUser + ":"
                + sauceKey + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities);

        /*String message = String.format("SauceOnDemandSessionID=%1$s job-name=%2$s", (((RemoteWebDriver) driver).getSessionId()).toString(), "SauceLabs Test");
        logger.info(message);*/
        logger.info("SauceOnDemandSessionID="+ (((RemoteWebDriver) driver).getSessionId()).toString() + " job-name=Job Test");

        return driver;
    }

    private void establishSauceConnect() {

        String path = new File("").getAbsolutePath().replaceAll("\\\\", "/")+PropertyManager.getProperty("sauce.connect.path");
        try {
            Runtime.getRuntime().exec("cmd.exe /c start "+path+" -u "+PropertyManager.getProperty("sauce.labs.username")+" -k "+PropertyManager.getProperty("sauce.labs.key"));
            Thread.sleep(60000);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
