package com.mckesson.mhs.cm.utils;

import com.mckesson.mhs.cm.tests.GenericTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.fluentlenium.core.filter.FilterConstructor.withText;

/**
 * This class contains all the common methods that could be used by any other
 * class method
 *
 * @author dsamantra
 */
public class CommonUtil extends GenericTest {

    public static final String currentDateAndTime = currentDateAndTime();

    /**
     * @param driver
     * @param tagName
     * @param expectedText
     * @return :to verify an element is present or not, depending on a given tag
     *         name and the text in it
     */

    public static boolean isTextPresent(WebDriver driver, String tagName,
                                        String expectedText) {
        boolean result = false;
        By by = By.xpath("//" + tagName + "[contains(text(),'" + expectedText
                + "')]");
        try {
            @SuppressWarnings("unused")
            WebElement w = driver.findElement(by);
            result = true;
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    public static boolean isTextPresentAnywhereOnPage(WebDriver driver, String expectedText) {
        boolean result = false;
        try {
            By by = By.cssSelector("BODY");
            result = driver.findElement(by).getText().matches("^[\\s\\S]*" + expectedText + "[\\s\\S]*$");
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    /**
     * @param driver
     * @param name
     * @return :to verify an element is present or not, depending on a given element name
     */

    public static boolean isElementByNamePresent(WebDriver driver, String name) {
        boolean result = false;
        try {
            @SuppressWarnings("unused")
            WebElement w = driver.findElement(By.name(name));
            result = true;
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    /**
     * @param driver
     * @param tagName
     * @param attrName
     * @param attrVal
     * @return :to verify an element is present or not, depending on a given element name
     */

    public static boolean isElementByTagAndAttributePresent(WebDriver driver, String tagName,
                                                            String attrName, String attrVal) {
        boolean result = false;
        try {
            @SuppressWarnings("unused")
            WebElement w = driver.findElement(By.cssSelector(tagName + "[" + attrName + "='" + attrVal + "']"));
            result = true;
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    /**
     * @returns : to find an element by text
     */

    public static WebElement findElementByTagAndText(WebDriver driver,
                                                     String tagName, String elementText) {

        WebElement txtElement = null;
        By by = By.xpath("//" + tagName + "[contains(text(),'" + elementText
                + "')]");

        txtElement = driver.findElement(by);
        return txtElement;

    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickElementByTagAndText(WebDriver driver,
                                                String tagName, String elementText) {

        waitForElementByTagAndText(driver, tagName, elementText);
        findElementByTagAndText(driver, tagName, elementText).click();
    }

    /**
     * @return : to wait till a given element arrives on the page
     */

    public static void waitForElementByTagAndText(final WebDriver driver,
                                                  final String tagName, final String elementText) {
        (new WebDriverWait(driver, 20))
                .until(new ExpectedCondition<WebElement>() {
                    // @Override
                    public WebElement apply(WebDriver d) {
                        return findElementByTagAndText(driver, tagName,
                                elementText);
                    }
                });
    }

    /**
     * @throws Exception
     * @returns : to find an element by its tag name and the value of a given
     * attribute
     */

    public static WebElement findElementByTagNameAndAttribute(WebDriver drive,
                                                              String valueOfElement, String tagName, String attributeName) {

        List<WebElement> ele = drive.findElements(By.tagName(tagName));
        WebElement webEle = null;

        for (int i = 0; i < ele.size(); i++) {
            if (ele.get(i).getAttribute(attributeName)
                    .equalsIgnoreCase(valueOfElement)) {
                webEle = ele.get(i);
                break;
            } else {
                webEle = null;
            }
        }
        return webEle;
    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     * @throws Exception
     */

    public static void clickElementByTagNameAndAttribute(WebDriver driver,
                                                         String valueOfElement, String tagName, String attributeName) {

        waitForElementByByTagNameAndAttribute(driver, valueOfElement, tagName,
                attributeName);
        findElementByTagNameAndAttribute(driver, valueOfElement, tagName,
                attributeName).click();
    }

    /**
     * @return : to wait till a given element arrives on the page
     */

    public static void waitForElementByByTagNameAndAttribute(
            final WebDriver driver, final String valueOfElement,
            final String tagName, final String attributeName) {
        (new WebDriverWait(driver, 60))
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver d) {
                        try {
                            return findElementByTagNameAndAttribute(driver,
                                    valueOfElement, tagName, attributeName);
                        } catch (Exception e) {
                            Reporter.log("Exception in findElementByTagNameAndAttribute() "
                                    + e);
                        }
                        return null;
                    }
                });

    }

    /**
     * @returns : to find an element by ID
     */

    public static WebElement findElementById(WebDriver drive, String elementID) {

        return drive.findElement(By.id(elementID));
    }

    /**
     * @returns : to find an element by Name
     */

    public static WebElement findElementByName(WebDriver drive,
                                               String elementName) {

        return drive.findElement(By.name(elementName));
    }

    /**
     * @return : to wait till a given element arrives on the page
     */

    public static void waitForElementByID(final WebDriver driver,
                                          final String elementID) {
        (new WebDriverWait(driver, 60))
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver d) {
                        return driver.findElement(By.id(elementID));
                    }
                });

    }

    /**
     * @return : to wait till a given element arrives on the page
     */

    public static void waitForElementByName(final WebDriver driver,
                                            final String elementName) {
        (new WebDriverWait(driver, 60))
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver d) {
                        return driver.findElement(By.name(elementName));
                    }
                });
    }

    /**
     * @return : to wait till a given element arrives on the page and then
     *         verify if its enabled or not
     */

    public static boolean verifyIfElementEnabled(final WebDriver driver,
                                                 final String elementID) {
        waitForElementByID(driver, elementID);
        return (findElementById(driver, elementID).isEnabled());

    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickIfElementIdPresent(final WebDriver driver,
                                               final String elementID) {
        waitForElementByID(driver, elementID);
        findElementById(driver, elementID).click();


    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickIfElementNamePresent(final WebDriver driver,
                                                 final String elementName) {

        waitForElementByName(driver, elementName);
        findElementByName(driver, elementName).click();
    }

    /**
     * @return : to wait till a given element arrives on the page and then type
     *         the provided value in it
     */

    public static void typeIfElementIdPresent(final WebDriver driver,
                                              final String elementID, String value) {

        waitForElementByID(driver, elementID);
        findElementById(driver, elementID).click();
        findElementById(driver, elementID).clear();
        findElementById(driver, elementID).sendKeys(value);

    }

    public static void typeIfFileElementIdPresent(final WebDriver driver,
                                                  final String elementID, String value) {

        waitForElementByID(driver, elementID);
        findElementById(driver, elementID).sendKeys(value);

    }

    /**
     * @return : to wait till a given element arrives on the page and then type
     *         the provided value in it
     */

    public static void typeIfElementNamePresent(final WebDriver driver,
                                                final String elementName, String value) {

        waitForElementByName(driver, elementName);
        findElementByName(driver, elementName).clear();
        findElementByName(driver, elementName).sendKeys(value);

    }

    /**
     * @param :driver
     * @param :elementID
     * @param :value
     * @return :selects an item from a list/drop down, if the list/drop id and
     *         item is given
     * @throws InterruptedException
     */

    public static void selectValByEleID(final WebDriver driver,
                                        final String elementID, String value) {

        // First, get the WebElement for the select tag
        waitForElementByID(driver, elementID);
        WebElement selectElement = driver.findElement(By.id(elementID));

        // Then instantiate the Select class with that WebElement
        Select select = new Select(selectElement);

        // Get a list of the options
        List<WebElement> options = select.getOptions();

        // For each option in the list, verify if it's the one you want and then
        // click it
        for (WebElement we : options) {
            if (we.getText().equals(value)) {
                we.click();
                break;
            }
        }
    }

    /**
     * @param :driver
     * @param :elementName
     * @param :value
     * @return :selects an item from a list/drop down, if the list/drop name and
     *         item is given
     * @throws InterruptedException
     */

    public static void selectValByEleName(final WebDriver driver,
                                          final String elementName, String value) {

        // First, get the WebElement for the select tag
        WebElement selectElement = driver.findElement(By.name(elementName));

        // Then instantiate the Select class with that WebElement
        Select select = new Select(selectElement);

        // Get a list of the options
        List<WebElement> options = select.getOptions();

        // For each option in the list, verify if it's the one you want and then
        // click it
        for (WebElement we : options) {
            if (we.getText().equals(value)) {
                we.click();
                break;
            }
        }
    }


    /**
     * @returns : to find an input element by preceding elements td and text
     */

    public static WebElement findInputEleByXPATH(WebDriver driver,
                                                 String precedingEleTxt) {

        String precedingEleTagName = "td";
        String elementType = "input";
        int elePosition = 1;
        WebElement txtElement = null;

        By by = By.xpath("//" + precedingEleTagName + "[contains(text(),'"
                + precedingEleTxt + "')]" + "/..//" + elementType + "["
                + elePosition + "]");

        txtElement = driver.findElement(by);
        return txtElement;

    }

    /**
     * @return : to wait till a given input element arrives on the page
     */

    public static void waitForInputEleByXPATH(final WebDriver driver,
                                              final String precedingEleTxt) {
        (new WebDriverWait(driver, 60))
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver d) {
                        return findInputEleByXPATH(driver, precedingEleTxt);
                    }
                });
    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickIfInputEleByXPATHPresent(final WebDriver driver,
                                                     final String precedingEleTxt) {

        waitForInputEleByXPATH(driver, precedingEleTxt);
        findInputEleByXPATH(driver, precedingEleTxt).click();
    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickXpathIfPrevEleTextPresent(final WebDriver driver,
                                                      final String precedingEleTag, final String precedingEleTxt) {

        String precedingEleTagName = precedingEleTag;
        String elementType = "input";
        int elePosition = 1;
        WebElement txtElement = null;

        By by = By.xpath("//" + precedingEleTagName + "[contains(text(),'"
                + precedingEleTxt + "')]" + "/..//" + elementType + "["
                + elePosition + "]");
        try {

            txtElement = driver.findElement(by);
            txtElement.click();

        } catch (Exception e) {
            e.printStackTrace();
            Reporter.log("Common Methods: No Element Found With Text :  "
                    + precedingEleTxt + e);
        }
    }

    /**
     * @return : to wait till a given element arrives on the page and then type
     *         the provided value in it
     */

    public static void typeIfInputEleByXPATHPresent(final WebDriver driver,
                                                    final String precedingEleTxt, String value) {

        waitForInputEleByXPATH(driver, precedingEleTxt);
        findInputEleByXPATH(driver, precedingEleTxt).click();
        findInputEleByXPATH(driver, precedingEleTxt).sendKeys(value);
    }

    /**
     * @returns : to find a select element by preceding elements preceding td
     * and text
     */

    public static WebElement findListBoxEleByXPATH(WebDriver driver,
                                                   String precedingEleTxt) {

        String precedingEleTagName = "td";
        String elementType = "select";
        int elePosition = 1;
        WebElement txtElement = null;

        By by = By.xpath("//" + precedingEleTagName + "[contains(text(),'"
                + precedingEleTxt + "')]" + "/..//" + elementType + "["
                + elePosition + "]");

        txtElement = driver.findElement(by);
        return txtElement;

    }

    /**
     * @return : to wait till a given input element arrives on the page
     */

    public static void waitForListBoxEleByXPATH(final WebDriver driver,
                                                final String precedingEleTxt) {
        (new WebDriverWait(driver, 60))
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(WebDriver d) {
                        return findListBoxEleByXPATH(driver, precedingEleTxt);

                    }
                });
    }

    /**
     * @return : to wait till a given element arrives on the page and then click
     *         on it
     */

    public static void clickIfListBoxEleByXPATHPresent(final WebDriver driver,
                                                       final String precedingEleTxt) {

        waitForListBoxEleByXPATH(driver, precedingEleTxt);
        findListBoxEleByXPATH(driver, precedingEleTxt).click();
    }

    /**
     * @return : to wait till a given element arrives on the page and then type
     *         the provided value in it
     */

    public static void selectValueIfListBoxEleByXPATHPresent(
            final WebDriver driver, final String precedingEleTxt, String value) {

        waitForListBoxEleByXPATH(driver, precedingEleTxt);
        findListBoxEleByXPATH(driver, precedingEleTxt).click();
        // Then instantiate the Select class with that WebElement
        Select select = new Select(findListBoxEleByXPATH(driver,
                precedingEleTxt));

        // Get a list of the options
        List<WebElement> options = select.getOptions();

        // For each option in the list, verify if it's the one you want and then
        // click it
        for (WebElement we : options) {
            if (we.getText().equals(value)) {
                we.click();
                break;
            }
        }
    }

    /**
     * @return the current date and time as String
     */

    public static String currentDateAndTime() {

        DateFormat df = new SimpleDateFormat("MMddyyyyHHmmss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        Reporter.log(reportDate);
        return reportDate;
    }

    public static String currentDateAndTimeforDateFormat(String format){
    	SimpleDateFormat df = new SimpleDateFormat();
    	String today=null;
		df.applyPattern(format);
		try {
			 Date date = Calendar.getInstance().getTime();
		     today = df.format(date);
			 Reporter.log(today +" is the todays date in give format "+format);
		} catch (Exception e) {
			
		}
		return today;
    }
    /**
     * @return :if the auto generated test-output folder exists, its renamed to
     *         the current date and time, to distinguish each test-output report
     *         according to execution
     * @throws :Exception
     */

    public static void renameFolder(String name) {

        File source = new File("test-output");
        File destination = new File(name+currentDateAndTime);

        if (source.exists()) {
            try {

                source.renameTo(destination);
                Reporter.log("FOLDER renamed from " + source + " to "
                        + destination);
            }
            catch (Exception e) {
                Reporter.log("Exception" + e);
            }
        } else {
            Reporter.log("FOLDER " + source + " does not exists");
        }
    }

    /**
     * @throws AWTException
     * @throws IOException
     */
    public static void captureScreen() throws IOException {

       driver           = new Augmenter().augment( driver );
       File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

// Now you can do whatever you need to do with it, for example copy somewhere
        FileUtils.copyFile(scrFile, new File("./" + "Test-Results-"
                + CommonUtil.currentDateAndTime()+".jpg"));
    }

    public static void sleepThread(long millisecs) {

        try {
            Thread.sleep(millisecs);
        } catch (InterruptedException e) {
            // Do nothing
        }
    }
    public static String getFilePath(String fileName) {
        return CommonUtil.class.getClassLoader().getResource(fileName).getPath();
    }
    
    public static void performMouseOver(WebElement element) throws AWTException{
		
//		Robot robot=new Robot();
//		robot.mouseMove(0, 0);
//
		Actions actions=new Actions(driver);
        actions.moveByOffset(0,0).perform();
		actions.moveToElement(element).perform();


	}
    
 public WebDriver navigateToWindowBasedOnURL(String Text){
	 	
	    initFluent(driver);
    	WebDriver popUpWindow = null;
    	String str=null;
    	Set<String>   windowSet=null;
    	boolean size=true;
    	int waitCount=0;
    	sleepThread(500);
    	windowSet = getDriver().getWindowHandles();
    	while(size){
    		if(windowSet.size()<2){
    			if(waitCount==20) break;
    			windowSet = getDriver().getWindowHandles();
    			size=true;
//    			System.out.println("windowset size !!!!!!!!!!-->"+windowSet.size());
    			sleepThread(1000);
    			waitCount=waitCount+1;
    		}else{
    			size=false;
    		}
    	}
//    	System.out.println("windowset size -->"+windowSet.size());
        if(windowSet.size()>1){
        	 for (String windowHandle : windowSet) {
             	try{
                 popUpWindow = getDriver().switchTo().window(windowHandle);
                 str=popUpWindow.getCurrentUrl();
                 if (str.contains(Text)) {
                     break;
                 }
             	}catch(Exception e){
             		System.out.println("No Such Window :"+windowHandle);
             	}
             }
        	
        }else{
        	Reporter.log("Pop window not displayed");
        }
       
        return popUpWindow;
    }
 
 public static boolean closeAllWindows(WebDriver driver){
	 Reporter.log("closeAllWindows()");
     
 	WebDriver popUpWindow = null;
 	String str=null;
 	Set<String> windowSet = driver.getWindowHandles();
 	Reporter.log("current windowset size -->"+windowSet.size());
     for (String windowHandle : windowSet) {
     	try{
         popUpWindow = driver.switchTo().window(windowHandle);
         str=popUpWindow.getCurrentUrl();
         popUpWindow.close();
     	}catch(Exception e){
     		Reporter.log("No Such Window :"+windowHandle);
     		return false;
     	}
     }
     return true;
 }
 public static boolean closeAWindow(String text,WebDriver driver){
	Reporter.log("closeAWindow("+text+")");
 	WebDriver popUpWindow = null;
 	String str=null;
 	if(driver!=null){
	 	Set<String> windowSet = driver.getWindowHandles();
	 	Reporter.log("current windowset size -->"+windowSet.size());
	     for (String windowHandle : windowSet) {
	     	try{
	         popUpWindow = driver.switchTo().window(windowHandle);
	         str=popUpWindow.getCurrentUrl();
		         if (str.contains(text)) {
		        	 Reporter.log("Found the window for closing -->"+str);
		        	 popUpWindow.close();
		        	 break;
		         }
	     	}catch(Exception e){
	     		Reporter.log("No Such Window :"+windowHandle);
	     		return false;
	     	}
	     }
 	}else{
 		Reporter.log("Driver is null");
 		return false;
 	}
     return true;
 }
 public void alertAcceptOrDismiss(String Value){
	 
	 Alert alert=null;
	 initFluent(driver);
	 
	 for(int i=0;i<=10;i++){
		 try{
			 alert=getDriver().switchTo().alert();
			 if(alert == null){
				 sleepThread(500);
			 }else{
				 break;
			 }
			 
		 }catch(Exception e){
			 sleepThread(500);
		 }
	 }
	 if (alert != null) {
			Reporter.log("Publish alert: " + alert.getText());
			if(Value.equalsIgnoreCase("accept")){
			   alert.accept();
			   Reporter.log("Alert accepted");
			}else{
				alert.dismiss();
				Reporter.log("Alert dismissed");
			}
	}
	sleepThread(500);
	Sync();
 }
 
 public void Sync(){
	 
	 initFluent(driver);
 	 sleepThread(50);
	 await().atMost(5000).untilPage().isLoaded();
 }

  public boolean navigateToMainPage(String page) { 
 	initFluent(driver);
 	if (page.equals("Documents")){
 		try{
 			CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Language")));
 		} catch (AWTException e){
 			e.printStackTrace();
 		}
	  	findFirst("a",withText("Documents")).click();
	  	return true;
 	} else if (page.equals("Contracts")){
 		try{
 		CommonUtil.performMouseOver(getDriver().findElement(By.linkText("Contracts")));
 		} catch (AWTException e){
 			e.printStackTrace();
 		}
 		findFirst("a",withText("Contracts")).click();
 		return true;
 	}
	 return false;
 }
}
