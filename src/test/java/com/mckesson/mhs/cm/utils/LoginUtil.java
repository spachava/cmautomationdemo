package com.mckesson.mhs.cm.utils;

import com.mckesson.mhs.cm.pageobjects.LoginPageObjects;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

/**
 * This Class contains the login/log off scenarios
 *
 * @author Siva Alla
 */

public class LoginUtil {

    /**
     * @param driver
     * @param username
     * @param password
     * @return : Code to Log into the application
     */
    public static void logIn(final WebDriver driver, final String username, final String password) {

        String envURL = PropertyManager.getProperty("contractmanager.url");
        driver.get(envURL);
        CommonUtil.typeIfElementIdPresent(driver,
                LoginPageObjects.USERNAME_TEXT_ID, username);
        CommonUtil.typeIfElementIdPresent(driver,
                LoginPageObjects.PASSWORD_TEXT_ID,
                password);
        CommonUtil.clickIfElementIdPresent(driver, LoginPageObjects.LOGIN_BTN_ID);

        Reporter.log("[[" + username + "  Logged in Successfully]]");
    }

    /**
     * @param driver
     * @return :Code to log off from the application
     */

    public static void logOff(final WebDriver driver) {
        CommonUtil.clickElementByTagAndText(driver, "span", LoginPageObjects.LOGOUT_SPAN_TEXT);
        Reporter.log("[[Logged out Successfully]]");
    }


    /**
     * @param driver
     * @return :Code to change Password from the application
     */

    public static void changePass(final WebDriver driver) {
        CommonUtil.clickElementByTagAndText(driver, "span", LoginPageObjects.CHANGE_SPAN_TEXT);

    }


}
