package com.mckesson.mhs.cm.utils;

import org.testng.Reporter;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 8 May, 2013
 * Time: 12:39:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropertyManager {

    private static Properties properties;

    static {
        properties = new Properties();
        try {
            properties.load(PropertyManager.class.getClassLoader().getResourceAsStream("project.properties"));
        } catch (IOException e) {
            System.err.println("Error loading properties file");
            Reporter.log("Error loading properties file");
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static String getProperty(String key){
        return properties.getProperty(key);
    }
}
