package com.mckesson.mhs.cm.utils;

/**
 * Created by spachava on 5/6/2014.
 */
public class Environments {

    private static String platform;
    private static String browser;
    private static String user;
    private static String key;
    private static String version;

    public static String getPlatform() {
        return platform;
    }

    public static void setPlatform(String platform) {
        Environments.platform = platform;
    }

    public static String getBrowser() {
        return browser;
    }

    public static void setBrowser(String browser) {
        Environments.browser = browser;
    }

    public static String getUser() {
        return user;
    }

    public static void setUser(String user) {
        Environments.user = user;
    }

    public static String getKey() {
        return key;
    }

    public static void setKey(String key) {
        Environments.key = key;
    }

    public static String getVersion() {
        return version;
    }

    public static void setVersion(String version) {
        Environments.version = version;
    }
}
