package com.mckesson.mhs.cm.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mckesson.mhs.cm.utils.PropertyManager.getProperty;

/**
 * Created by spachava on 4/28/2014.
 */
public class EnvironmentUtil extends Environments{

    public DesiredCapabilities getCapabilitiesList(DesiredCapabilities capabilities){

        if (System.getenv("SELENIUM_PLATFORM")!=null)
            setTCEnvironments(capabilities);

        else if (System.getenv("SELENIUM_BROWSER")!=null) {
                Map<String, String> list = getEnviroments("SELENIUM_BROWSER");

                setEnvironments(capabilities);
            } else
                setEnvironments(capabilities);

        capabilities.setCapability("name", "Sauce Sample Test");
        if (System.getenv("SELENIUM_PLATFORM")!=null)
            capabilities.setCapability(CapabilityType.PLATFORM,
                    System.getenv("SELENIUM_PLATFORM"));
        else {
            capabilities.setCapability(CapabilityType.PLATFORM,
                    getProperty("operating.system"));
            setSystemProxy();
        }

        if(System.getenv("SELENIUM_BROWSER")!=null)
            capabilities.setCapability(CapabilityType.BROWSER_NAME,
                    System.getenv("SELENIUM_BROWSER"));
        else
            capabilities.setCapability(CapabilityType.BROWSER_NAME,
                    getProperty("browser.name"));

        if (System.getenv("SELENIUM_VERSION") != null)
            capabilities.setCapability(CapabilityType.VERSION,
                    System.getenv("SELENIUM_VERSION"));
        else
            capabilities.setCapability(CapabilityType.VERSION,
                    getProperty("browser.version"));

        if(System.getenv("SAUCE_USER_NAME") != null)
            capabilities.setCapability("sauceUser", System.getenv("SAUCE_USER_NAME"));
        else
            capabilities.setCapability("sauceUser", getProperty("sauce.labs.username"));

        if(System.getenv("SAUCE_API_KEY") != null)
            capabilities.setCapability("sauceKey", System.getenv("SAUCE_API_KEY"));
        else
            capabilities.setCapability("sauceKey", getProperty("sauce.labs.key"));

        return capabilities;
    }

    private static void setTCEnvironments(DesiredCapabilities capabilities){
        capabilities.setCapability("name", "Sauce Sample Test");
        capabilities.setCapability(CapabilityType.PLATFORM,
                System.getenv("SELENIUM_PLATFORM"));
        capabilities.setCapability(CapabilityType.BROWSER_NAME,
                System.getenv("SELENIUM_BROWSER"));
        capabilities.setCapability(CapabilityType.VERSION,
                System.getenv("SELENIUM_VERSION"));
        capabilities.setCapability("sauceUser", System.getenv("SAUCE_USER_NAME"));
        capabilities.setCapability("sauceKey", System.getenv("SAUCE_API_KEY"));
    }

    private static void setEnvironments(DesiredCapabilities capabilities){
        capabilities.setCapability("name", "Sauce Sample Test");
        capabilities.setCapability(CapabilityType.PLATFORM,
                getProperty("operating.system"));
        capabilities.setCapability(CapabilityType.BROWSER_NAME,
                getProperty("browser.name"));
        capabilities.setCapability(CapabilityType.VERSION,
                getProperty("browser.version"));
        capabilities.setCapability("sauceUser", getProperty("sauce.labs.username"));
        capabilities.setCapability("sauceKey", getProperty("sauce.labs.key"));
    }

    private static Map<String, String> getEnviroments(String str) {
        Map<String, String> array = new HashMap<String, String>();
        try {
            JSONArray jsonArray = new JSONArray(str);
            int i = jsonArray.length();

            for (int j = 0; j < i; j++) {
                List<String> list = new ArrayList<String>();

                JSONObject object = jsonArray.getJSONObject(j);

                array.put("Set"+j, "" +object);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }

    private static void setSystemProxy() {
        System.setProperty("http.proxyHost", "172.16.100.111");
        System.setProperty("http.proxyPort", "8080");
        System.setProperty("http.proxyUser", "spachava");
        System.setProperty("http.proxyPassword", "Sr1n1vas1");
    }

    public static String getJSONString() {
        String str;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("platform", getPlatform());
            jsonObject.put("browser", getBrowser());
            jsonObject.put("browser-version", getVersion());
            jsonObject.put("username", getUser());
            jsonObject.put("access-key", getKey());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        str = "[" + jsonObject.toString()+"]";
        return str;
    }

    public static void setCapabilities(DesiredCapabilities capabilities){
        if (System.getenv("SELENIUM_PLATFORM")!=null)
            setTCEnvironments(capabilities);
        else {
            setEnvironments(capabilities);
            setSystemProxy();
        }
    }

/*    public static DesiredCapabilities setCapabilities(JSONObject object) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        try {
            capabilities.setCapability(CapabilityType.PLATFORM,
                    object.get("platform"));
            capabilities.setCapability(CapabilityType.BROWSER_NAME,
                    object.get("browser"));
            capabilities.setCapability(CapabilityType.VERSION,
                    object.get("browser-version"));
            capabilities.setCapability("sauceUser", getProperty("sauce.labs.username"));
            capabilities.setCapability("sauceKey", getProperty("sauce.labs.key"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return capabilities;
    }*/
}
