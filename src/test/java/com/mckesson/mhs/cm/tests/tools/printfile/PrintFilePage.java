package com.mckesson.mhs.cm.tests.tools.printfile;

import static org.fest.assertions.Assertions.assertThat;

import org.fluentlenium.core.FluentPage;

public class PrintFilePage extends FluentPage {
	 @Override
	    public String getUrl() {
	        return "/template/tools%2CCreatePdfsMain.vm";
	    }

	    @Override
	    public void isAt() {
	        assertThat(find("td.contentTitle").getText()).endsWith("Create Print File");
	    }
	    
}

