package com.mckesson.mhs.cm.tests.tools.printfile;

import com.mckesson.mhs.cm.tests.GenericTest;
import com.mckesson.mhs.cm.utils.CommonUtil;
import com.mckesson.mhs.cm.utils.CommonWorkFlowUtil;
import org.fluentlenium.core.annotation.Page;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.mckesson.mhs.cm.utils.CommonUtil.captureScreen;
import static org.fest.assertions.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.with;
import static org.fluentlenium.core.filter.FilterConstructor.withName;

// Manual TestCases file name CM - Print File.xls
public class PRTFILE001_Tools_CreatePrintFile extends GenericTest {

	
	@Page
	public PrintFilePage page;

    CommonUtil CommonUtil1=new CommonUtil();
    CommonWorkFlowUtil CommonWorkFlowUtil=new CommonWorkFlowUtil();

    @BeforeSuite
    public void before() throws Exception {

    	CommonWorkFlowUtil.openApplication();
    	
    }

    @AfterSuite
    public void after() throws AWTException, IOException {
    	
    	CommonWorkFlowUtil.closeApplication();
    	
    }

	@Test(enabled=true)
	public void test1NoException() throws AWTException {

		CommonUtil1.Sync();
		goTo("/template/tools%2CCreatePdfsMain.vm");
	  	CommonUtil1.Sync();
	}

	@Test(enabled=true)
	public void test2cancelcretaePrintFileTest() throws IOException, AWTException {
		WebDriver popUpWindow = null;
    	String parentWindow=null;
		parentWindow = getDefaultDriver().getWindowHandle();
        captureScreen();
		click("#btnMainCreate");
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Create_Pdfs");
        captureScreen();
		popUpWindow.manage().window().maximize();
			
		assertThat(find("td.formcontentData").getTexts().contains("PDF"));

		await().atMost(5, TimeUnit.SECONDS).until("#pdf").areDisplayed();
		find("td.formcontentData").findFirst("input").click();
        captureScreen();
		find("span", with("title").contains("Cancel")).click();
		driver.switchTo().window(parentWindow);
		assertThat(find("#listViewContainer").find("#createPdfsMain").find("thead").find("tr").size() == 1);
	}

	@Test(enabled=false)
	public void test3createPrintFileTest() throws IOException, AWTException {
		WebDriver popUpWindow = null;
    	String parentWindow=null;
		parentWindow = getDefaultDriver().getWindowHandle();
		click("#btnMainCreate");
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("Create_Pdfs");
		
		popUpWindow.manage().window().maximize();
				
		assertThat(find("td.formcontentData").getTexts().contains("PDF"));
		await().atMost(5, TimeUnit.SECONDS).until("#pdf").areDisplayed();
		find("td.formcontentData").findFirst("input").click();
		await().atMost(5, TimeUnit.SECONDS).until("#btnAddContract").areDisplayed();
		parentWindow = driver.getWindowHandle();
		click("#btnAddContract");
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("ContractChooser");
		
		CommonWorkFlowUtil.performSearch("Contract Name", "TestCreatePrintFile");
		CommonWorkFlowUtil.applyFilter("Status", "Approved");
        captureScreen();
		find("input", withName("selectionids")).click();
		click("#btnChooseSelected");

		getDriver().switchTo().window(parentWindow);

		CommonUtil.sleepThread(5000);
		
		find("input", 2, with("type").contains("checkbox"),
				withName().contains("selectionids")).click();
		parentWindow = getDriver().getWindowHandle();
		click("#btnRemove");
		CommonUtil1.alertAcceptOrDismiss("accept");
		
		getDriver().switchTo().window(parentWindow);

		click("#btnSave");
        captureScreen();
		await().atMost(20, TimeUnit.SECONDS).until("#dialogMessage").isPresent();
		assertThat(find("#dialogMessage").contains("Generation successfully completed!")).isNotNull();

        click("#dialogMessage");
        captureScreen();
		getDriver().switchTo().window(parentWindow);

	}

	@Test(enabled=false)
	public void test4filterAndFolderNamePrintFileTest() {
		goTo(page);
		CommonUtil1.Sync();
		click("#btnApply");
		
		assertThat(find("td", with("columnid").contains("name")).find("span")
				.getTexts().contains(
						new SimpleDateFormat("MMM d- yyyy").format(new Date())));
		
	}

	@Test(enabled=false)
	public void test5outputPrintFileTest() {
		WebDriver popUpWindow = null;
		Set<String> windowSet = null;
		String parentWindow = driver.getWindowHandle();
		find("td", with("columnid").contains("name")).find("span").click();// BatchView
		popUpWindow=CommonUtil1.navigateToWindowBasedOnURL("BatchView");
		assertThat(find("div.contentListDiv").contains(".pdf")).isNotNull();
		popUpWindow.close();
		getDriver().switchTo().window(parentWindow);
		goTo(page);
		CommonUtil1.Sync();
		

	}

}
