package com.mckesson.mhs.cm.tests;

import com.mckesson.mhs.cm.utils.PropertyManager;
import org.fluentlenium.adapter.FluentTestNg;
import org.fluentlenium.core.Fluent;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;

import java.util.concurrent.TimeUnit;

//import static com.mckesson.mhs.cm.driver.TestExecutionDriver.*;
/**
 * Created by IntelliJ IDEA.
 * User: Siva Nagi Reddy Alla
 * Date: 27 Dec, 2012
 * Time: 4:59:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericTest extends FluentTestNg {

    public static WebDriver driver;

    public void setDriver(WebDriver webDriver){
        driver = webDriver;
    }

    @Override
    public WebDriver getDefaultDriver() {
        return driver;
    }

    @AfterClass
    @Override
    public void afterClass() {
        // Do nothing
    }

    @Override
    public String getDefaultBaseUrl() {
        return PropertyManager.getProperty("contractmanager.url");
    }

    @Override
    public Fluent withDefaultPageWait(long l, TimeUnit timeUnit) {
        long timeout = Long.parseLong(PropertyManager.getProperty("default.page.timeout"));
        return super.withDefaultPageWait(timeout, TimeUnit.SECONDS);
    }

    @Override
    public Fluent withDefaultSearchWait(long l, TimeUnit timeUnit) {
        long timeout = Long.parseLong(PropertyManager.getProperty("default.search.timeout"));
        return super.withDefaultSearchWait(timeout, TimeUnit.SECONDS);   
    }
}
