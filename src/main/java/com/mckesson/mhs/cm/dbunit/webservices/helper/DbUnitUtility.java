package com.mckesson.mhs.cm.dbunit.webservices.helper;

import com.mckesson.mhs.cm.dbunit.webservices.ExportdataSet;
import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.ext.oracle.OracleDataTypeFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbUnitUtility {
	private static Logger logger = Logger.getLogger(ExportdataSet.class);

	public static String getPropertyValue(String proertyKey) {
		String propertyvalue = null;
		try {
			InputStream in = DbUnitUtility.class.getClassLoader()
					.getResourceAsStream("dbUnitDatabase.proerties");
			Properties prop = new Properties();
			prop.load(in);
			propertyvalue = prop.getProperty(proertyKey);

		} catch (IOException ie) {
			logger.debug("problem while loading file");
		}
		return propertyvalue;
	}

	public static IDatabaseConnection getConnection() throws DatabaseUnitException,
			ClassNotFoundException, SQLException {
		Class.forName(getPropertyValue("driverClass"));

        String url = getPropertyValue("url");
        String username = getPropertyValue("username");
		Connection jdbcConnection = DriverManager.getConnection(
                url, username, getPropertyValue("password"));

        IDatabaseConnection connection = null;
        if(url.contains("oracle")){
            connection = new DatabaseConnection(jdbcConnection, username.toUpperCase());
            DatabaseConfig dbCfg = connection.getConfig();
            dbCfg.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
        }else{
            connection = new DatabaseConnection(jdbcConnection);
        }

		logger.info("dbunit database connection successful");
		return connection;

	}


}
