package com.mckesson.mhs.cm.dbunit.webservices;

import com.mckesson.mhs.cm.dbunit.webservices.helper.DbUnitUtility;
import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

public class ExportdataSet {
	private static Logger logger = Logger.getLogger(ExportdataSet.class);
	
	public static void main(String[] args) {
		exportDataSet();
	}

	private static void exportDataSet() {
		IDatabaseConnection connection = null;
		try {
			connection = DbUnitUtility.getConnection();;
			// exportPartialData(tableNames, connection);
			exportFullData(connection);
		} catch (Exception e) {
			logger.error("Exception while exporting data", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Exception while dbunit connection", e);
				}
			}
		}

	}

	static void exportFullData(IDatabaseConnection connection)
			throws DataSetException, SQLException, FileNotFoundException,
			IOException {
		ITableFilter filter = new DatabaseSequenceFilter(connection);
		IDataSet dataset = new FilteredDataSet(filter, connection
				.createDataSet());

		FlatXmlDataSet.write(dataset, new FileOutputStream(new File(".")
				.getAbsolutePath()
				+ "\\src\\test\\resources\\originalData.xml"));
		logger.info("Exported original Data dtabase");
	}
}
